package
{
	import com.bit101.components.Style;
	
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Vector3D;
	
	import main.MakerMain;
	import main.ToolMain;
	
	import views.AlertPanel;
	
	[SWF(width=1000,height=600,frameRate=30)]
	/**
	 * @author Ado
	 */
	public class TrackMaker3D extends Sprite
	{
		private var maker:MakerMain;
		private var tool:ToolMain;
		public function TrackMaker3D()
		{
			if(stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event=null):void
		{
			Style.embedFonts = false;
			Style.fontName = "Microsoft Yahei";
			Style.fontSize = 11;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.stage.addEventListener(Event.RESIZE, onResize);
			//3d制作主体
			maker = new MakerMain();
			addChild(maker);
			//2D界面主体
			tool = new ToolMain();
			addChild(tool);
			//Alert面板容器传入
			AlertPanel.getInstance().container = this.stage;
			
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, onExit);
		}
		/**
		 * 退出的时候回收系统资源 
		 * @param e
		 * 
		 */		
		private function onExit(e:Event):void
		{
			//TODO 退出的时候需要进行相关的回收处理
			trace("Exiting..");
		}
		/**
		 * 改变尺寸的时候调整相关的的尺寸和位置 
		 * @param e
		 * 
		 */		
		private function onResize(e:Event):void
		{
			maker.onResize();
			tool.resize();
		}
	}
}