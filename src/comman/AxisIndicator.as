package comman
{
	import away3d.containers.Scene3D;
	import away3d.entities.SegmentSet;
	import away3d.primitives.LineSegment;
	import away3d.primitives.WireframePlane;
	
	import flash.geom.Vector3D;
	/**
	 * 3d空间坐标指示器
	 * 包括3条坐标系指示线，1个XZ坐标平面 
	 * @author ado
	 * 
	 */	
	public class AxisIndicator
	{
		private var _scene:Scene3D;
		private var original:Vector3D;
		
		private var xAxis:LineSegment;
		private var yAxis:LineSegment;
		private var zAxis:LineSegment;
		private var axises:SegmentSet;
		private var segmentPlane:WireframePlane;
		
		private var _visible:Boolean;
		public function AxisIndicator(scene:Scene3D)
		{
			_scene = scene;
			init();
		}
		private function init():void
		{
			var original:Vector3D = new Vector3D();
			xAxis = new LineSegment(original,new Vector3D(1000,0,0),0xff);
			xAxis.thickness = 5;
			yAxis = new LineSegment(original, new Vector3D(0,1000,0),0xff00);
			yAxis.thickness = 5;
			zAxis = new LineSegment(original, new Vector3D(0,0,1000),0xff0000);
			zAxis.thickness = 5;
			axises = new SegmentSet();
			axises.addSegment(xAxis);
			axises.addSegment(yAxis);
			axises.addSegment(zAxis);
			segmentPlane = new WireframePlane(5000,5000,10,10, 0xffffff,1,"xz");
		}
		public function get visible():Boolean
		{
			return _visible;
		}
		/**
		 * 设置是否要在舞台上显示 
		 * @param value
		 * 
		 */		
		public function set visible(value:Boolean):void
		{
			_visible = value;
			if(value)
			{
				_scene.addChild(axises);
				_scene.addChild(segmentPlane);
			}else
			{
				_scene.removeChild(axises);
				_scene.removeChild(segmentPlane);
			}
		}
	}
}