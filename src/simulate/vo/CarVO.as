package simulate.vo
{
	public class CarVO
	{
		/**
		 *  最大速度：加速的极限
		 */		
		public var maxSpeed:Number=0;
		/**
		 * 加速度 ：加速的因素
		 */		
		public var accelate:Number=0;
		/**
		 * 当前速度 
		 */		
		public var currentSpeed:Number=0;
		/**
		 * id 
		 */		
		public var id:int=1;
		/**
		 * 刹车 ：减速的因素
		 */		
		public var brake:Number=0;
		/**
		 * 最小速度:减速的极限
		 */		
		public var minSpeed:Number = 0;
		public var minRndSpeed:Number = 0;
		public function CarVO()
		{
		}
		public function carConfig():XML
		{
			var xml:XML = XML("<car />");
			xml.@id = id;
			xml.@maxSpeed = maxSpeed*1e-3;//milseconds
			xml.@accelate = accelate*1e-6;//milseconds
//			xml.@currentSpeed = currentSpeed*1e-3;//milseconds
			xml.@brake = brake*1e-6;//milseconds
			xml.@minSpeed = minSpeed*0.001;//milseconds
			return xml;
		}
	}
}