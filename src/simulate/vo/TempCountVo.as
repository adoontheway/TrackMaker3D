package simulate.vo
{
	import com.sections.vo.SectionBaseVO;
	
	import flash.geom.Vector3D;

	/**
	 * 模拟系统需要使用的临时参数缓存vo 
	 * @author ado
	 * 
	 */	
	public class TempCountVo
	{
		/**
		 * 当前段可达到的最高速度 
		 */		
		public var currentMaxSpeed:Number;
		/**
		 * 总行驶距离：保留值
		 */		
		public var totalDist:Number;
		/**
		 * 各部分的保留数组 
		 */		
		public var sectionArr:Array = [];
		/**
		 * 车的配置 xml root
		 */		
		public var carConfig:XML;
		/**
		 * 当前速度 
		 */		
		public var currentSpeed:Number = 0;
		/**
		 * 当前花费时间 
		 */		
		public var currentTime:Number = 0;
		/**
		 * 当前角度 
		 */		
		public var currentAngle:Number = 0;
		/**
		 * 实际参考时间 
		 */		
		public var referTime:Number = 0;
		/**
		 * 理论上使用时间 
		 */		
		public var costTime:Number = 0;
		/**
		 * 备用时间 
		 */		
		public var bearTime:Number = 0;
		/**
		 * 当前加速度 
		 */		
		public var curAccelerate:Number;
		/**
		 * 偏移角度 
		 */		
		public var offset_angle:Number = 0;
		/**
		 * 总量:(t+v/a)^2 = v^2 + 2*s*a/a^2; t+v/a的根 
		 */		
		public var totalMount:Number = 0;
		/**
		 * 局部当前id 
		 */		
		public var id:int = -1;
		/**
		 * 局部临时位置变量0 
		 */		
		public var vec0:Vector3D = new Vector3D();
		/**
		 * 局部临时位置变量1 
		 */	
		public var vec1:Vector3D = new Vector3D();
		/**
		 * 局部临时位置变量2 
		 */	
		public var vec2:Vector3D = new Vector3D();
		/**
		 * y斜角 :弧度
		 */		
		public var yAngle:Number;
		/**
		 * 局部临时变量 
		 */		
		public var sectionVo:SectionBaseVO;
		public function TempCountVo()
		{
		}
	}
}