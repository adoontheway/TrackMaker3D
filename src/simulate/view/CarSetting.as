package simulate.view
{
	import com.bit101.components.Label;
	import com.bit101.components.Text;
	
	import flash.display.Sprite;
	
	import simulate.vo.CarVO;
	
	public class CarSetting extends Sprite
	{
		//Label 组
		private var maxSpeedLbl:Label;
		private var accelerateLbl:Label;
		private var brakeLbl:Label;
		private var minSpeedLbl:Label;
		private var minRndSpeed:Label;
		//Text组
		private var maxSpeedTxt:Text;
		private var accelerateTxt:Text;
		private var brakeTxt:Text;
		private var minSpeedTxt:Text;
		private var minRndSpdTxt:Text;
		public function CarSetting()
		{
			super();
			initComponents();
		}
		private function initComponents():void
		{
			maxSpeedLbl = new Label(this,5,5,"最大速度");
			accelerateLbl = new Label(this,5,30,"加速度");
			brakeLbl = new Label(this,5,55,"制动");
			minSpeedLbl = new Label(this, 5, 80,"最小速度");
			minRndSpeed = new Label(this,5,105,"弯道最小速");
			
			maxSpeedTxt = new Text(this,maxSpeedLbl.width+10,5,"1500");
			maxSpeedTxt.height = 25;
			maxSpeedTxt.width = 60;
			accelerateTxt = new Text(this,maxSpeedTxt.x,30,"400");
			accelerateTxt.height = 25;
			accelerateTxt.width = 60;
			brakeTxt = new Text(this, maxSpeedTxt.x, 55,"100");
			brakeTxt.height = 25;
			brakeTxt.width =  60;
			minSpeedTxt = new Text(this,maxSpeedTxt.x,80,"700");
			minSpeedTxt.height = 25;
			minSpeedTxt.width = 60;
			minRndSpdTxt = new Text(this,minSpeedTxt.x,105,"200");
			minRndSpdTxt.height = 25;
			minRndSpdTxt.width = 60;
		}
		/**
		 * 面板配置的车配置 
		 * @return 
		 * 
		 */		
		public function getCar():CarVO
		{
			var car:CarVO = new CarVO();
			car.maxSpeed = Number(maxSpeedTxt.text);
			car.accelate = Number( accelerateTxt.text);
			car.brake = Number(brakeTxt.text);
			car.minSpeed = Number(minSpeedTxt.text);
			car.minRndSpeed = Number(minRndSpdTxt.text);
			return car;
		}
	}
}