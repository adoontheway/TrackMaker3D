package simulate.view
{
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.bit101.components.Text;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import simulate.SimulateSystem;
	import simulate.vo.CarVO;
	
	public class SimulatePanel extends Sprite
	{
		private var addBtn:PushButton;
		private var group:HBox;
		private var startBtn:PushButton;
		private var carItems:Vector.<CarSetting>;
		private var gravityLbl:Label;
		private var gravityTxt:Text;
		public function SimulatePanel()
		{
			super();
			
			carItems = new Vector.<CarSetting>;
			initView();
			updateBg();
		}
		private function initView():void
		{
			group = new HBox(this,5,30);
			addBtn = new PushButton(this,5,2,"添加汽车", onAdd);
			startBtn = new PushButton(this,addBtn.width+10,2,"开始", onStart);
			gravityLbl = new Label(this, startBtn.x+startBtn.width+5, 2,"重力");
			gravityTxt = new Text(this, gravityLbl.x+gravityLbl.width+5,2,"9.8");
			gravityTxt.width = 50;
			gravityTxt.height = 20;
		}
		private function onAdd(e:MouseEvent):void
		{
			if(group.numChildren < 3)
			{
				var carSetting:CarSetting = new CarSetting();
				var temp:CarSetting = group.addChild(carSetting) as CarSetting;
				carItems.push(temp);
				this.x = this.stage.stageWidth - this.width >> 1 ;
				this.y = this.stage.stageHeight - this.height >> 1;
				updateBg();
			}
		}
		private function onStart(e:MouseEvent):void
		{
			var cars:Vector.<CarVO> = new Vector.<CarVO>();
			for each(var c:CarSetting in carItems)
			{
				cars.push(c.getCar());
			}
			if(cars.length == 0)
			{
				trace("无车.....");
				return;
			}
			SimulateSystem.simulate(cars,Number(gravityTxt.text));
			this.parent.removeChild(this);
		}
		private function updateBg():void
		{
			this.graphics.clear();
			this.graphics.beginFill(0xcccccc,0.8);
			this.graphics.drawRoundRect(0,0,width+10,height+10,10,10);
			this.graphics.endFill();
		}
	}
}