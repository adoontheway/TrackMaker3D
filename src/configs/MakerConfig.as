package configs
{
	import away3d.materials.TextureMaterial;
	import away3d.primitives.CubeGeometry;

	/**
	 * @author Ado
	 */
	public class MakerConfig
	{
		/**
		 * 1度相等的弧度 
		 */		
		public static const UNIT_ANGLE:Number = Math.PI/180;
		/**
		 * 赛道宽度 
		 */		
		public static const TRACK_WIDTH:Number = 500;
		/**
		 * 长条形赛道 
		 */		
		public static const RECT_TYPE:String = "rect";
		/**
		 * 弧形赛道 
		 */		
		public static const ROUND_TYPE:String = "round";
		/**
		 * 赛道材质
		 */		
		public static var TRACK_MATERIAL:TextureMaterial;
		/**
		 * 路线关键点：普通 
		 */		
		public static const NORMALPOINT:String = "np";
		/**
		 * 路线关键点：内弯点 
		 */		
		public static const NMLRNDINNER:String = "sp";
		/**
		 * 路线关键点：漂移点 
		 */		
		public static const RNDSLDINNER:String = "sdp";
		/**
		 * 路线关键点：普通弯点 
		 */		
		public static const NMLRNSTART:String = "rp";
		/**
		 * 导出为直线路段 
		 */		
		public static const EX_AS_LINE:int = 1;
		/**
		 * 导出为普通弧形路段 
		 */	
		public static const EX_AS_ROUND:int = 2;
		/**
		 * 导出为漂移弧形路段 
		 */	
		public static const EX_AS_SLIP:int = 3;
		
		/**
		 * 根据类型取得关键点的色值 
		 * @param t
		 * @return 
		 * 
		 */		
		public static function getColor(t:String):uint
		{
			if(t == NORMALPOINT)
			{
				return 0xff00;
			}else if(t == NMLRNDINNER)
			{
				return 0xff;
			}else if(t == RNDSLDINNER)
			{
				return 0xff0000;
			}else if(t == NMLRNDINNER)
			{
				return 0x9900ff;
			}
			return 0;
		}
	}
}