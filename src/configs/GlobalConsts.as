package configs
{
	/**
	 * @author Ado
	 */
	public class GlobalConsts
	{
		/**
		 * 路面素材 
		 */		
		[Embed(source="../assets/Road_Tile.jpg")]
		public static var RoadTile:Class;
		/**
		 * -3次方 
		 */		
		public static const MILLIUNIT:Number = 1e-3;
		/**
		 * -6次方 
		 */		
		public static const POWMILIUNIT:Number = 1e-6;
		
	}
}