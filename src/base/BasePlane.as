package base
{
	/**
	 * @author Ado
	 */
	import away3d.core.base.Geometry;
	import away3d.entities.Mesh;
	import away3d.materials.ColorMaterial;
	import away3d.materials.MaterialBase;
	import away3d.primitives.PlaneGeometry;
	import away3d.primitives.SphereGeometry;
	
	import configs.MakerConfig;
	
	import flash.geom.Vector3D;
	
	import geo.SectorGeometry;
	
	public class BasePlane extends Mesh
	{
		public var id:int = 0;
		
		protected var _startPoint:Mesh;
		protected var _endPoint:Mesh;
		protected var _pointGeo:SphereGeometry;
		protected var _pointMaterial:ColorMaterial;
			
		protected var _startNode:Vector3D;
		protected var _endNode:Vector3D;
		public function BasePlane(geometry:Geometry, material:MaterialBase=null)
		{
			super(geometry, material);
			_startNode = new Vector3D();
			_endNode = new Vector3D();
			
			_pointGeo = new SphereGeometry(10);
			_pointMaterial = new ColorMaterial(Math.random()*0xffffff);
			_startPoint = new Mesh(_pointGeo,_pointMaterial);
			_endPoint = new Mesh(_pointGeo,_pointMaterial);
		}
		public function countNode():void
		{
			_startPoint.position = _startNode;
			_endPoint.position = _endNode;
		}
		public function get StartNode():Vector3D
		{
			return _startNode;
		}
		public function get EndNode():Vector3D
		{
			return _endNode;
		}
		public function planeXML():XML
		{
			return null;
		}

		public function get startPoint():Mesh
		{
			return _startPoint;
		}

		public function get endPoint():Mesh
		{
			return _endPoint;
		}
		/**
		 * 根据传入的xml创建赛道 
		 * @param xml
		 * @return 
		 * 
		 */		
		public static function createPlane(xml:XML):BasePlane
		{
			if(xml.@type == "rect")
			{
				var geo:Geometry = new PlaneGeometry(Number(xml.@width), MakerConfig.TRACK_WIDTH);
				var plane:BasePlane = new RectPlane(geo,MakerConfig.TRACK_MATERIAL);
			}else
			{
				geo = new SectorGeometry();
				plane = new RoundPlane(geo,MakerConfig.TRACK_MATERIAL);
				SectorGeometry(plane.geometry).radius = xml.@radius;
				SectorGeometry(plane.geometry).angle = xml.@angle;
			}
			plane.id = int(xml.@id);
			plane.rotationY = Number(xml.@rotationY);
			plane.rotationZ = Number(xml.@rotationZ);
			plane.rotationX = Number(xml.@rotationX);
			plane.x = Number(xml.@x);
			plane.y = Number(xml.@y); 
			plane.z = Number(xml.@z);
			
			return plane;
		}
		
	}
}