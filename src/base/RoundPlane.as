package base
{
	/**
	 * @author Ado
	 */
	import away3d.core.base.Geometry;
	import away3d.core.math.MathConsts;
	import away3d.materials.MaterialBase;
	
	import configs.MakerConfig;
	
	import geo.SectorGeometry;
	
	public class RoundPlane extends BasePlane
	{
		public function RoundPlane(geometry:Geometry, material:MaterialBase=null)
		{
			super(geometry, material);
		}
		/**
		 * (x-a)^2 + (y-b)^2 + (z-d)^2 = r^2 
		 * 
		 */		
		override public function countNode():void
		{
			var tempgeo:SectorGeometry = this.geometry as SectorGeometry;
			
			var r:Number = tempgeo.radius - (MakerConfig.TRACK_WIDTH >> 1);
			var startAngle:Number = tempgeo.rotation * MathConsts.DEGREES_TO_RADIANS;
			
//			var rX:Number = rotationX*MathConsts.DEGREES_TO_RADIANS;
			var rY:Number = rotationY*MathConsts.DEGREES_TO_RADIANS;
//			var rZ:Number = rotationZ*MathConsts.DEGREES_TO_RADIANS;
			
			_startNode.x = x + r * Math.cos(startAngle-rY);//*Math.cos(rZ)
			_startNode.y = y ;//* Math.sin(rZ)
			_startNode.z = z + r * Math.sin(startAngle-rY);//*Math.cos(rZ)
			
			var endAngle:Number = (tempgeo.rotation+tempgeo.angle)*MathConsts.DEGREES_TO_RADIANS;
			
			_endNode.x = x + r * Math.cos(endAngle-rY);//*Math.cos(rX)*Math.cos(rZ)
			_endNode.y = y ;//r* Math.sin(rX) * Math.sin(rZ)
			_endNode.z = z + r * Math.sin(endAngle-rY);//*Math.cos(rX)*Math.sin(rZ)
			
			
			super.countNode();
		}
		override public function planeXML():XML
		{
			var xml:XML = XML("<mesh />");
			xml.@id = id;
			xml.@type = MakerConfig.ROUND_TYPE;
			xml.@radius =SectorGeometry(this.geometry).radius;
			xml.@x = x;
			xml.@y = y;
			xml.@z = z;
			xml.@rotationX = 0;
			xml.@rotationY = rotationY;
			xml.@rotationZ = 0;
			xml.@angle = SectorGeometry(this.geometry).angle;
			return xml;
		}
	}
}