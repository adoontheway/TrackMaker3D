package base
{
	/**
	 * @author Ado
	 */
	import away3d.core.base.Geometry;
	import away3d.core.math.MathConsts;
	import away3d.materials.MaterialBase;
	import away3d.primitives.PlaneGeometry;
	
	import configs.MakerConfig;
	
	import flash.geom.Vector3D;
	
	public class RectPlane extends BasePlane
	{
		public function RectPlane(geometry:Geometry, material:MaterialBase=null)
		{
			super(geometry, material);
		}
		/**
		 * 用圆形方程求
		 * 
		 */		
		override public function countNode():void
		{
			var r:Number = (this.geometry as PlaneGeometry).width >> 1;
			var deltaX:Number = r * Math.cos(-MathConsts.DEGREES_TO_RADIANS*rotationY)*Math.cos(MathConsts.DEGREES_TO_RADIANS*rotationZ);
			var deltaY:Number = r * Math.sin(MathConsts.DEGREES_TO_RADIANS*rotationZ)*Math.cos(-MathConsts.DEGREES_TO_RADIANS*rotationY);
			var deltaZ:Number = r * Math.sin(-MathConsts.DEGREES_TO_RADIANS*rotationY)*Math.cos(MathConsts.DEGREES_TO_RADIANS*rotationZ);
			
			_startNode.x = x - deltaX;
			_startNode.y = y - deltaY;
			_startNode.z = z - deltaZ;
			_endNode.x = x + deltaX;
			_endNode.y = y + deltaY;
			_endNode.z = z + deltaZ;
			super.countNode();
		}
		
		override public function planeXML():XML
		{
			var xml:XML = XML("<mesh />");
			xml.@id = id;
			xml.@type = MakerConfig.RECT_TYPE;
			xml.@width = PlaneGeometry(this.geometry).width;
			xml.@x = x;
			xml.@y = y;
			xml.@z = z;
			xml.@rotationX = rotationX;
			xml.@rotationY = rotationY;
			xml.@rotationZ = rotationZ;
			return xml;
		}
	}
}