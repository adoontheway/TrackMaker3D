package base
{
	import away3d.core.base.Geometry;
	import away3d.entities.Mesh;
	import away3d.materials.ColorMaterial;
	import away3d.materials.MaterialBase;
	import away3d.primitives.CubeGeometry;
	
	import configs.MakerConfig;

	/**
	 * Cube
	 * @author Ado
	 */
	public class TrackPoint extends Mesh
	{
		private var _type:String = "";
		public var id:int;
		public function TrackPoint(geo:CubeGeometry,material:ColorMaterial=null)
		{
			super(geo,material);
			_type = MakerConfig.NORMALPOINT;
			if(material == null)
				this.material = new ColorMaterial(MakerConfig.getColor(_type));
		}

		public function get type():String
		{
			return _type;
		}

		public function set type(value:String):void
		{
			if(_type == value)
			{
				return;
			}
			_type = value;
			ColorMaterial(material).color = MakerConfig.getColor(value);
		}
		/**
		 * 路线信息 
		 * @return 
		 * 
		 */		
		public function pathConfig():XML
		{
			var xml:XML = XML("<keypoint />");
			xml.@id = id;
			xml.@type = _type;
			xml.@x = x;
			xml.@y = y;
			xml.@z = z;
			xml.@rotationZ = rotationZ;
			return xml;
		}
		/**
		 * 根据传入的xml配置文件创建新的关键点对象 
		 * @param xml
		 * @return 
		 * 
		 */		
		public static function createPoint(xml:XML):TrackPoint
		{
			var point:TrackPoint = new TrackPoint(new CubeGeometry);
			point.id = int(xml.@id);
			point.type = xml.@type;
			var cMaterial:ColorMaterial = new ColorMaterial(MakerConfig.getColor(xml.@type));
			point.material = cMaterial;
			point.x = int(xml.@x);
			point.y = int(xml.@y);
			point.z = int(xml.@z);
			point.rotationZ = int(xml.@rotationZ);
			return point;
		}
	}
}