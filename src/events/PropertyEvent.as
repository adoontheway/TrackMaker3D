package events
{
	/**
	 * @author Ado
	 */
	import flash.events.Event;
	
	public class PropertyEvent extends Event
	{
		/**
		 * 增加元素 
		 */		
		public static const ADD_ELEMENT:String = "addElement";
		/**
		 * 移除元素 
		 */		
		public static const REMOVE_ELEMENT:String = "removeElement";
		/**
		 * 更改元素属性 
		 */		
		public static const ELEMENT_PROP_CHANGE:String = "changeElementPropety";
		/**
		 * 选择目标 
		 */		
		public static const SELECT_ELEMENT:String = "selectElement";
		/**
		 * 绘制路线 
		 */		
		public static const DRAW_PATH:String = "drawPath";
		/**
		 * 检验节点 
		 */		
		public static const CHECK_NODES:String = "checkNodes";
		/**
		 * 显示坐标系 
		 */		
		public static const SHOW_AXIS:String = "showAxis";
		/**
		 * 保存路面配置信息 
		 */		
		public static const SAVE_PLANE:String = "savePlane";
		/**
		 * 保存路线 
		 */		
		public static const SAVE_PATH:String = "savePath";
		/**
		 *  显示组合面板界面
		 */		
		public static const SHOW_COMBINE_PANEL:String = "showConbinePanel";
		/**
		 * 缝合俩界面 
		 */		
		public static const COMBINE_PANEL:String = "combinePanel";
		/**
		 * 导入路线 
		 */		
		public static const IMPORT:String = "importConfig";
		/**
		 * 选中路线关键点 
		 */		
		public static const SELECT_POINT:String = "selectPoint";
		/**
		 * 模拟汽车
		 */		
		public static const SIMULATE:String = "simulation";
		/**
		 * 画圆心 
		 */		
		public static const DRAW_CENTRE:String = "drawString";
		/**
		 * 摄像机复位到原点 
		 */		
		public static const RESET_CAMERA:String = "resetCamera";
		private var _data:Object;
		public function PropertyEvent(type:String, d:Object=null,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_data = d;
		}
		/**
		 * 事件携带的数据 
		 * @return 
		 * 
		 */		
		public function get data():Object
		{
			return _data;
		}
		override public function clone():Event
		{
			return new PropertyEvent(this.type,_data,this.bubbles,this.cancelable);
		}
		override public function toString():String
		{
			return "[PropertyEvent(type="+this.type+")]";
		}
	}
}