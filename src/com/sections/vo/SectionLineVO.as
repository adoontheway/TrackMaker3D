package com.sections.vo
{
	import flash.geom.Vector3D;
	
	/**
	 * 直线段VO
	 * 
	 * @custom 
	 * <author email="scutterry&#64;vip.qq.com">scutterry</author>
	 * <create>Sep 12, 2012</create>
	 * <modify>Sep 12, 2012</modify>
	 * <version>1.0</version>
	 */
	
	public class SectionLineVO extends SectionBaseVO
	{
		/**
		 * 偏移角度，根据这个来算出路径长度 
		 */		
		public var oAng:Number;
		
		/**
		 * 相对偏移起始角度 
		 */		
		public var sAng:Number;
		
		/**
		 * 方向，一定要normalize
		 */		
		public var direction:Vector3D;
		
		public function SectionLineVO()
		{
			super();
			direction = new Vector3D();
			type = 1;
		}
		override public function toXML():XML
		{
			var xml:XML = super.toXML();
			//修正坐标系
			xml.@o_ang = -this.oAng;
			xml.@s_ang = -this.sAng;
			
			xml.@x = direction.x;
			xml.@y = direction.y;
			xml.@z = direction.z;
			return xml;
		}
	}//end class
}