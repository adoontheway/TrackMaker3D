package  com.sections.vo
{
	import flash.geom.Vector3D;
	
	/**
	 * 路线段基类
	 * 
	 * @custom 
	 * <author email="scutterry&#64;vip.qq.com">scutterry</author>
	 * <create>Sep 12, 2012</create>
	 * <modify>Sep 12, 2012</modify>
	 * <version>1.0</version>
	 */
	
	public class SectionBaseVO
	{
		/**
		 * 距离 
		 */		
		public var d:Number = 0;
		/**
		 * id 
		 */		
		public var id:int = 0;
		/**
		 * 类型 
		 */		
		public var type:int = 0;
		/**
		 * 初始速度 
		 */		
		public var v:Number = 0;
		/**
		 * 加（减）速度 
		 */		
		public var a:Number = 0;
		
		/**
		 * 段的最小速度 
		 */		
		public var minV:Number = 0;
		/**
		 * 时间毫秒 
		 */		
		public var t:int = 0;
		
		/**
		 * 开始时间 
		 */		
		public var beginTime:int = 0;
		/**
		 * 开始位置 
		 */		
		public var startPos:Vector3D;
		/**
		 * y 轴夹角 
		 */		
		public var yRad:Number = 0;
		
		public function SectionBaseVO()
		{
			startPos = new Vector3D();
		}
		
		public function toXML():XML
		{
			var xml:XML =XML("<section />");
			xml.@d = d;
			xml.@id = id;
			xml.@type = type;
			xml.@v = v;
			xml.@a = a;
			xml.@min_v = minV;
			xml.@t = t;
			xml.@sx = startPos.x;
			xml.@sy = startPos.y;
			xml.@sz = startPos.z;
			xml.@y_rad = yRad;
			return xml;
		}
	}//end class
}