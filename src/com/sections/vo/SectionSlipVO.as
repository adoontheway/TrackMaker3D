package com.sections.vo
{
	
	/**
	 * 漂移滑行段VO
	 * 
	 * @custom 
	 * <author email="scutterry&#64;vip.qq.com">scutterry</author>
	 * <create>Sep 12, 2012</create>
	 * <modify>Sep 12, 2012</modify>
	 * <version>1.0</version>
	 */
	
	public class SectionSlipVO extends SectionRoundVO
	{
		public var slipAngle:Number=0;
		
		public var angleSpeed:Number;
		public function SectionSlipVO()
		{
			super();
			type = 3;
		}
		override public function toXML():XML
		{
			var xml:XML = super.toXML();
			xml.@slip_ang = slipAngle;
			//anglespeed
			return xml;
		}
	}//end class
}