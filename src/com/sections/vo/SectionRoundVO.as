package com.sections.vo
{
	import flash.geom.Vector3D;
	
	/**
	 * 转弯段VO
	 * 
	 * @custom 
	 * <author email="scutterry&#64;vip.qq.com">scutterry</author>
	 * <create>Sep 12, 2012</create>
	 * <modify>Sep 12, 2012</modify>
	 * <version>1.0</version>
	 */
	
	public class SectionRoundVO extends SectionBaseVO
	{
		/**
		 * 圆心 
		 */		
		public var centerPoint:Vector3D;
		
		/**
		 * 扇形赛道的旋转角度 
		 */		
		public var spinAng:Number;
		
		/**
		 * 半径 
		 */		
		public var r:Number;
		
		/**
		 * 扇形赛道的初始角度 
		 */		
		public var rAng:Number;
		
		public function SectionRoundVO()
		{
			super();
			centerPoint = new Vector3D();
			type = 2;
		}
		override public function toXML():XML
		{
			var xml:XML = super.toXML();
			xml.@r = r;
			xml.@r_ang = rAng;
			xml.@spin_ang = spinAng;
			xml.@x = centerPoint.x;
			xml.@y = centerPoint.y;
			xml.@z = centerPoint.z;
			return xml;
		}
	}//end class
}