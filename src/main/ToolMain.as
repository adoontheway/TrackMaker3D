package main
{
	/**
	 * @author Ado
	 */
	import away3d.entities.Mesh;
	
	import events.PropertyEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import managers.EventManager;
	
	import simulate.view.SimulatePanel;
	
	import views.CombinPanel;
	import views.CoordinateTool;
	import views.PointProperty;
	import views.SysTool;
	
	public class ToolMain extends Sprite
	{
		private var coorTool:CoordinateTool;
		private var sysTool:SysTool;
		private var combinePanel:CombinPanel;
		private var pointProperty:PointProperty;
		private var simulatePanel:SimulatePanel;
		public function ToolMain()
		{
			this.mouseEnabled = false;
			if(stage)
			{
				init();
			}else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		private function init(e:Event=null):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, init);
			initViews();
			resize();
			EventManager.getInstance().addEventListener(PropertyEvent.SHOW_COMBINE_PANEL, showCombinePanel);
			EventManager.getInstance().addEventListener(PropertyEvent.SIMULATE, onSimulate);
		}
		
		private function initViews():void
		{
			coorTool = new CoordinateTool();
			addChild(coorTool);
			
			sysTool = new SysTool();
			addChild(sysTool);
			
			combinePanel = new CombinPanel();
			
			pointProperty = new PointProperty();
			pointProperty.y = this.stage.stageHeight - pointProperty.height - 250;
			addChild(pointProperty);
			
			simulatePanel = new SimulatePanel();
		}
		private function showCombinePanel(e:PropertyEvent):void
		{
			this.addChild(combinePanel);
			combinePanel.x = this.stage.stageWidth - combinePanel.width; 
			combinePanel.y = this.stage.stageHeight - combinePanel.height - 20;
		}
		private function onSimulate(e:PropertyEvent):void
		{
			if(this.contains(simulatePanel))
			{
				this.removeChild(simulatePanel);
			}else
			{
				this.addChild(simulatePanel);
				simulatePanel.x = this.stage.stageWidth - simulatePanel.width >> 1;
				simulatePanel.y = this.stage.stageHeight - simulatePanel.height >> 1;
			}
		}
		public function resize():void
		{
			sysTool.x = this.stage.stageWidth - sysTool.width;
			sysTool.y = this.stage.stageHeight - sysTool.height;
		}
	}
}