package views
{
	/**
	 * @author Ado
	 */
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class Instruction extends Sprite
	{
		private var txt:TextField;
		public function Instruction()
		{
			super();
			init();
			this.addEventListener(MouseEvent.CLICK, onClicked);
		}
		private function init():void
		{
			txt = new TextField();
			txt.width = 450;
			txt.wordWrap = true;
			txt.selectable = false;
			
			txt.htmlText = "<t/><b>使用说明</b>";
			txt.htmlText += "1: A W S D可以控制摄像头前后左右移动；";
			txt.htmlText += "2: 选中目标后可以通过方向键对它进行移动；";
			addChild(txt);
			
			this.graphics.beginFill(0xcccccc,0.8);
			this.graphics.drawRoundRect(-5,-5,txt.width+10, txt.height+10,10,10);
			this.graphics.endFill();
		}
		private function onClicked(e:MouseEvent):void
		{
			if(this.parent)
			{
				this.parent.removeChild(this);
			}
		}
	}
}