package views
{
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.bit101.components.Text;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class AlertPanel extends Sprite
	{
		private var con:DisplayObjectContainer;
		
		private var titleLbl:Label;
		private var contentTxt:Text;
		private var confirmBtn:PushButton;
		
		private static var _instance:AlertPanel;
		//回调函数
		private var callBack:Function;
		//回调参数
		private var param:Object;
		public function AlertPanel()
		{
			super();
			if(_instance == null)
			{
				_instance = this;
				initView();
			}else 
			{
				trace("请用单例....");
			}
		}
		private function initView():void
		{
			titleLbl = new Label(this,150,5,"警告");
			
			contentTxt = new Text(this,5,30,"content");
			contentTxt.width = 200;
			contentTxt.height = 150;
			contentTxt.textField.textColor = 0xaa1100;
			contentTxt.mouseChildren = false;
			contentTxt.mouseEnabled = false;
			confirmBtn = new PushButton(this,100,contentTxt.height+contentTxt.y+5,"确定", hide);
			
			this.graphics.beginFill(0xcccccc,0.8);
			this.graphics.drawRoundRect(0,0,this.width+20,this.height+20,10,10);
			this.graphics.endFill();
		}
		/**
		 * 警告 
		 * @param message	警告信息
		 * @param cb		确定按钮的回调函数
		 * @param cParam	回调参数
		 * 
		 */		
		public function alert(message:String, cb:Function=null, cParam:Object=null):void
		{
			con.addChild(_instance);
			contentTxt.text = message;
			callBack = cb;
			param = cParam;
			_instance.x = _instance.stage.stageWidth - _instance.width >> 1;
			_instance.y = _instance.stage.stageHeight - _instance.height >> 1;
		}
		private function hide(e:MouseEvent):void
		{
			this.parent.removeChild(this);
			if(callBack != null)
			{
				if(param)
					callBack(param);
				else
					callBack();
				
				callBack = null;
				param = null;
			}
		}
		public static function getInstance():AlertPanel
		{
			if(_instance == null)
			{
				_instance = new AlertPanel();
			}
			return _instance;
		}
		/**
		 * 给警告面板设定容器 
		 * @param con
		 * 
		 */		
		public function set container(con:DisplayObjectContainer):void
		{
			_instance.con = con;
		}
	}
}