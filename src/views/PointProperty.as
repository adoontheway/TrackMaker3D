package views
{
	import base.TrackPoint;
	
	import com.bit101.components.ComboBox;
	import com.bit101.components.Label;
	import com.bit101.components.Text;
	
	import configs.MakerConfig;
	
	import events.PropertyEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import managers.EventManager;
	
	public class PointProperty extends Sprite
	{
		private var _target:TrackPoint;
		
		private var typeLbl:Label;
		private var typeBox:ComboBox;
		private var idLbl:Label;
		private var idTxt:Text;
		public function PointProperty()
		{
			super();
			initView();
		}
		private function initView():void
		{
			typeLbl = new Label(this,5,5,"类型");
			typeBox = new ComboBox(this,typeLbl.width+10,5,"",
				[
				{label:"普通点",value:MakerConfig.NORMALPOINT},
				{label:"普通弯点",value:MakerConfig.NMLRNSTART},
				{label:"内弯点",value:MakerConfig.NMLRNDINNER},
				{label:"漂移点",value:MakerConfig.RNDSLDINNER}
			]);
			idLbl = new Label(this,0,30,"id");
			idLbl.height = 25;
			idLbl.width = 10;
			
			idTxt = new Text(this,typeBox.x,30,"");
			idTxt.height = 25;
			idTxt.width = 80;
			idTxt.textField.restrict = "0-9";
			
			typeBox.addEventListener(Event.SELECT, onSelected);
			idTxt.addEventListener(Event.CHANGE, onIdChange);
			
			EventManager.getInstance().addEventListener(PropertyEvent.SELECT_POINT, onPoint);
		}
		private function onIdChange(e:Event):void
		{
			_target.id = int(idTxt.text);
		}
		private function onPoint(e:PropertyEvent):void
		{
			_target = e.data as TrackPoint;
			if(_target)
			{
				idTxt.text = _target.id.toString();
				var items:Array = typeBox.items;
				for each(var i:Object in items)
				{
					if(i.value == _target.type)
					{
						var index:int = items.indexOf(i);
						typeBox.selectedIndex = index;
						break;
					}
				}
			}
		}
		private function onSelected(e:Event):void
		{
			if(_target)
				_target.type = typeBox.selectedItem.value;
		}
	}
}