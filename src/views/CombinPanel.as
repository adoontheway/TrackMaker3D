package views
{
	/**
	 * @author Ado
	 */
	import com.bit101.components.PushButton;
	import com.bit101.components.RadioButton;
	
	import events.PropertyEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import managers.EventManager;
	
	public class CombinPanel extends Sprite
	{
		private var ssCheck:RadioButton;
		private var seCheck:RadioButton;
		private var esCheck:RadioButton;
		private var eeCheck:RadioButton;
		private var applyBtn:PushButton;
		private var combineType:String;
		private var canclBtn:PushButton;
		public function CombinPanel()
		{
			super();
			init();
		}
		private function init():void
		{
			ssCheck = new RadioButton(this, 10, 5, "首-首缝合",true, onSelected);
			seCheck = new RadioButton(this,10,40,"首-尾缝合",false, onSelected);
			esCheck = new RadioButton(this,10,75,"尾-首缝合",false, onSelected);
			eeCheck = new RadioButton(this,10, 110,"尾-尾缝合",false, onSelected);
			ssCheck.groupName = "combine";
			seCheck.groupName = "combine";
			esCheck.groupName = "combine";
			eeCheck.groupName = "combine";
			combineType = ssCheck.name = "ss";
			seCheck.name = "se";
			esCheck.name = "es";
			eeCheck.name = "ee";
			applyBtn = new PushButton(this,25, 145,"组合", onApply);
			canclBtn = new PushButton(this,25,180, "取消", onCancel);
			this.graphics.beginFill(0xcccccc,0.8);
			this.graphics.drawRoundRect(0,0,160,220,10,10);
			this.graphics.endFill();
		}
		private function onSelected(e:Event):void
		{
			e.stopImmediatePropagation();
			combineType = RadioButton(e.target).name;
		}
		private function onApply(e:MouseEvent):void
		{
			e.stopImmediatePropagation();
			onCancel();
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.COMBINE_PANEL,combineType));
		}
		private function onCancel(e:MouseEvent=null):void
		{
			if(e)
				e.stopImmediatePropagation();
			this.parent.removeChild(this);
		}
	}
}