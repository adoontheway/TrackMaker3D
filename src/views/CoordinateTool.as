package views
{
	
	import away3d.entities.Mesh;
	import away3d.primitives.PlaneGeometry;
	
	import base.BasePlane;
	
	import com.bit101.components.ComboBox;
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.bit101.components.Slider;
	import com.bit101.components.Text;
	
	import events.PropertyEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import geo.SectorGeometry;
	
	import managers.EventManager;

	/**
	 * 放在上面的坐标和旋转控制工具
	 * @author Ado
	 */
	public class CoordinateTool extends Sprite
	{
		///////通用的属性///////
		private var xCoord:Text;
		private var yCoord:Text;
		private var zCoord:Text;
		private var xRotation:Slider;
		private var yRotation:Slider;
		private var zRotation:Slider;
		
		private var xLabel:Label;
		private var yLabel:Label;
		private var zLabel:Label;
		private var xrLabel:Label;
		private var yrLabel:Label;
		private var zrLabel:Label;
		//////////矩形赛道才有的属性//////////////////
		private var widthLabel:Label;
		private var widthTxt:Text;
		//////////扇形赛道才有的属性//////////////////
		private var radiusLabel:Label;
		private var radiusTxt:Text;
		private var angleLabel:Label;
		private var angleTxt:Text;
		
		private var addLabel:Label;
		private var choseType:ComboBox;
		private var confirmBtn:PushButton;
		
		private var instrucBtn:PushButton;
		
		
		private var _target:BasePlane;
		
		private var instruction:Instruction;
		
		public function CoordinateTool()
		{
			initViews();
			EventManager.getInstance().addEventListener(PropertyEvent.SELECT_ELEMENT, selectTarget);
		}
		private function initViews():void
		{
			xLabel = new Label(this,10,15,"X:");
			xCoord = initText(55, 10);
			
			yLabel = new Label(this,10,45,"Y:");
			yCoord = initText(55, 40);
			
			zLabel = new Label(this,10,75,"Z:");
			zCoord = initText(55, 70);
			
			xrLabel = new Label(this,10,105,"RotationX:");
			xRotation = initSlider(55,100, onRotation);
			
			yrLabel = new Label(this,10,135,"RotationY:");
			yRotation = initSlider(55,130, onRotation);
			
			zrLabel = new Label(this,10,165,"RotationZ:");
			zRotation = initSlider(55,160, onRotation);
			
			widthLabel = new Label(this,10,195,"Width");
			widthTxt = initText(55,190);
			
			radiusLabel = new Label(this, 10, 225, "Radius:");
			radiusTxt = initText(55,220);
			
			angleLabel = new Label(this, 10, 255, "Span:");
			angleTxt = initText(55, 250);
			
			addLabel = new Label(this,560,15,"Add Plane");
			choseType = new ComboBox(this,620,10,"rect",[{label:"rect"},{label:"round"}]);
			choseType.width = 60;
			choseType.selectedIndex = 0;
			confirmBtn = new PushButton(this,680,10,"Add", onAdd);
			confirmBtn.width = 50;
			
			instrucBtn = new PushButton(this, 740, 10,"Instruction", onInstruct);
			instrucBtn.width = 60;
			
			instruction = new Instruction();
		}
		
		private function initSlider(xpos:Number, ypos:Number, handler:Function):Slider
		{
			var slider:Slider = new Slider("horizontal",this,xpos,ypos,handler);
			slider.maximum = 180;
			slider.minimum = -180;
			slider.value = 0;
			return slider;
		}
		
		private function initText(xpos:Number,ypos:Number):Text
		{
			var txt:Text = new Text(this, xpos, ypos, "0");
			txt.height = 25;
			txt.width = 50;
			txt.textField.restrict = "[0-9 . -]";
			txt.addEventListener(Event.CHANGE, onChange);
			return txt;
		}
		private function selectTarget(e:PropertyEvent):void
		{
			_target = e.data as BasePlane;
			if(_target == null)
			{
				this.mouseEnabled = false;
				this.mouseChildren = false;
			}else
			{
				xCoord.text = _target.x.toString();
				yCoord.text = _target.y.toString();
				zCoord.text = _target.z.toString();
				
				yRotation.value = _target.rotationY;
				
				if(_target.geometry is PlaneGeometry)
				{
					xRotation.value = _target.rotationX;
					zRotation.value = _target.rotationZ;
					widthTxt.text = (_target.geometry as PlaneGeometry).width.toString();
					makableTxt(widthTxt, true);
					makableTxt(radiusTxt);
					makableTxt(angleTxt);
					makableTxt(xRotation,true);
					makableTxt(zRotation,true);
				}else
				{
					radiusTxt.text = (_target.geometry as SectorGeometry).radius.toString();
					angleTxt.text = (_target.geometry as SectorGeometry).angle.toString();
					makableTxt(widthTxt);
					makableTxt(radiusTxt,true);
					makableTxt(angleTxt, true);
					makableTxt(xRotation);
					makableTxt(zRotation);
				}
				
			}
		}
		private function makableTxt(txt:Object,able:Boolean=false):void
		{
			txt.mouseEnabled = able;
			txt.mouseChildren = able;
		}
		private function onChange(e:Event):void
		{
			if(_target == null) return;
			if(e.target == xCoord)
			{
				var value:Number = Number(xCoord.text);
				_target.x = value;
			}else if(e.target == yCoord)
			{
				value = Number(yCoord.text);
				_target.y = value;
			}else if(e.target == zCoord)
			{
				value = Number(zCoord.text);3
				_target.z = value;
			}
			else if(e.target == widthTxt)
			{
				(_target.geometry as PlaneGeometry).width = Number(widthTxt.text);
			}else if(e.target == radiusTxt)
			{
				(_target.geometry as SectorGeometry).radius = Number(radiusTxt.text);
			}else if(e.target == angleTxt)
			{
				(_target.geometry as SectorGeometry).angle = Number(angleTxt.text);
			}
			_target.countNode();
			
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.CHECK_NODES));
		}
		private function onAdd(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.ADD_ELEMENT,choseType.selectedItem.label));
		}
		private function onInstruct(e:MouseEvent):void
		{
			this.stage.addChild(instruction);
			instruction.x = this.stage.stageWidth - instruction.width >> 1;
			instruction.y = this.stage.stageHeight - instruction.height >> 1;
		}
		
		private function onRotation(e:Event):void
		{
			e.stopImmediatePropagation();
			if(_target == null) return;
			if(e.target == xRotation)
			{
				_target.rotationX = xRotation.value;
			}else if(e.target == yRotation)
			{
				_target.rotationY = yRotation.value;
			}else if(e.target == zRotation)
			{
				_target.rotationZ = zRotation.value;
			}
			_target.countNode();
		}
	}
}