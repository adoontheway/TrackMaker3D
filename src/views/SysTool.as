package views
{
	/**
	 * @author Ado
	 */
	import com.bit101.components.CheckBox;
	import com.bit101.components.PushButton;
	
	import events.PropertyEvent;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import managers.EventManager;
	
	public class SysTool extends Sprite
	{
		private var savePath:PushButton;
		private var savePlane:PushButton;
		private var draw:PushButton;
		private var axisBox:CheckBox;
		private var importPlane:PushButton;
		private var importPath:PushButton;
		private var simulateBtn:PushButton;
		private var resetBtn:PushButton;
		public function SysTool()
		{
			super();
			initButtons();
		}
		private function initButtons():void
		{
			axisBox = new CheckBox(this, 0,0,"Show Axis", onAxisCheck);
			axisBox.selected = true;
			draw = new PushButton(this,axisBox.width+5,0,"绘制路线", onDraw);
			savePath = new PushButton(this,draw.x+draw.width+2,0,"保存路线",onPath);
			savePlane = new PushButton(this,savePath.x+savePath.width+2,0,"保存路径", onPlane);
			importPath = new PushButton(this,savePlane.x+savePlane.width+2,0,"导入...",onImport);
			simulateBtn = new PushButton(this,importPath.x+importPath.width+2,0,"模拟线路",onSimulate);
			resetBtn = new PushButton(this,simulateBtn.x+simulateBtn.width+2,0,"复位摄像机",onReset);
		}
		private function onReset(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.RESET_CAMERA));
		}
		private function onSimulate(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.SIMULATE));
		}
		private function onAxisCheck(e:MouseEvent):void
		{
			 EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.SHOW_AXIS));
		}
		private function onPath(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.SAVE_PATH));
		}
		private function onPlane(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.SAVE_PLANE));
		}
		private function onDraw(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.DRAW_PATH));
		}
		private function onImport(e:MouseEvent):void
		{
			EventManager.getInstance().dispatchEvent(new PropertyEvent(PropertyEvent.IMPORT));
		}
	}
}