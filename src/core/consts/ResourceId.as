package core.consts
{
	/**
	 * 
	 * @author Ado
	 **/
	public class ResourceId
	{
		/**
		 * 主面板工具条：包括聊天框
		 */		
		public static const MAINBAR:uint = 1;
		/**
		 * 人物 
		 */		
		public static const PEOPLE:uint = 2;
		/**
		 * 背包 
		 */		
		public static const BAG:uint = 3;
		/**
		 * 任务 
		 */		
		public static const TASK:uint = 4;
	}
}