package  core.consts
{
	/**
	 * 面板类型类，实例化没有实际意义
	 * @author Ado
	 */
	public class PanelTypes
	{
		/**
		 * 共存 :可同时出现多个
		 */		
		public static const SHARE:String = "share";
		/**
		 * 独占 :屏蔽其他所有面板
		 */		
		public static const MONOPOLIZE:String = "monoplize";
		/**
		 * 排斥 :不允许其他面板同时存在
		 */		
		public static const EXCLUDE:String = "exclude";
	}
}