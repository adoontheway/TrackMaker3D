package core.errors
{
	/**
	 * 工具类实例化错误：工具类不可被实例化，此为实例化工具类的时候抛出的异常
	 * @author Ado
	 */
	public class UtilClassInstanceError extends Error
	{
		public function UtilClassInstanceError(className:*="", id:*=0)
		{
			super(className+" can't be instantiated...", id);
		}
	}
}