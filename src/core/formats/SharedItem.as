package core.formats
{
	/**
	 * 本地存储对象格式
	 * @author Ado
	 * @see core.utils.LocalCache
	 * @example
	 * <listing>
	 * var obj:Object = {key:value};
	 * //初始化共享数据
	 * var si:ShareItem = new ShareItem();
	 * //设置版本号：默认1.0
	 * si.version = 1.1;
	 * //存放的key值
	 * si.key = "xxx";
	 * //存放的资源
	 * si.data = obj;
	 * //存放到本地缓存
	 * LocalCacheManager.saveToLocal(si);
	 * </listing>
	 */
	public class SharedItem
	{
		/**
		 * 版本号 
		 * @param value
		 * 
		 */
		public var version:Number;
		/**
		 * 存儲的key值 
		 * @param value
		 * 
		 */	
		public var key:String;
		/**
		 * 存储的數據 
		 * @param value
		 * 
		 */	
		public var data:Object;
		public function SharedItem(obj:Object=null)
		{
			if(obj==null)
				return;
			else 
				init(obj);
		}
		//初始化
		private function init(value:Object):void
		{
			for(var key:String in value)
			{
				if(this.hasOwnProperty(key))
				{
					this[key]=value[key];
				}
			}
		}
	}
}