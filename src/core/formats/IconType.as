package core.formats
{
	import core.errors.UtilClassInstanceError;

	/**
	 * 聊天表情图标，不可实例化
	 * @throws UtilClassInstanceError
	 * @author Ado
	 */
	public class IconType
	{
		/**
		 * 本地编译的MC格式
		 * */
		public static  const TYPE_MOVIECLIP:String = "movieClip";
		/**
		 * 图片格式 
		 */		
		public static  const TYPE_JPG:String = "jpg";
		/**
		 * 外部链接的swf格式 
		 */		
		public static  const TYPE_SWF:String = "swf";
		
		public function IconType()
		{
			throw new  UtilClassInstanceError("IconType");
		}
	}//end class
}