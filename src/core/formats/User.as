package core.formats
{
	/**
	 * 玩家信息
	 * @author Ado
	 */
	public class User
	{
		/**
		 * 昵称 
		 */		
		public var name:String = "";
		/**
		 * id 
		 */		
		public var id:int = 0;
		/**
		 * 帮派id，0为无帮派 
		 */		
		public var clanId:int = 0;
		/**
		 * 门派id，0为无门派 
		 */		
		public var schoolId:int = 0;
		/**
		 * 性别，男为0， 女为1 
		 */		
		public var gender:int = 0;
		public function User()
		{
		}
	}
}