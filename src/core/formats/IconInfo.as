package core.formats
{
	/**
	 * 聊天表情Icon的数据对象格式
	 * @author Ado
	 * @see IconType
	 */
	public class IconInfo
	{
		/**
		 * 表情所在的index
		 * */
		public var index:int = -1;
		/**
		 * 表情对应的字符串
		 * */
		public var iconStr:String = "";
		/**
		 * 表情对应的地址
		 * */
		public var iconUrl:String = "";
		/**
		 * 表情对应的类型
		 * */
		public var iconType:String = "";
		public function IconInfo(obj:Object=null)
		{
			if(obj) init(obj);
		}
		
		private function init(value:Object):void
		{
			for(var key:String in value)
			{
				if(this.hasOwnProperty(key))
				{
					this[key]=value[key];
				}
			}
		}
	}//end class
}