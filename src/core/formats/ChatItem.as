package core.formats
{
	/**
	 * 聊天信息单位
	 * @author Ado
	 */
	public class ChatItem
	{
		/**
		 * 聊天信息发送者 
		 */		
		public var name:String = "";
		/**
		 * 发送者的id 
		 */		
		public var id:int = 0;
		/**
		 * 信息内容 
		 */		
		public var content:String = "";
		/**
		 * 性别
		 * @see core.managers.GlobalConfig.MALE 
		 * @see core.managers.GlobalConfig.FEMALE
		 */		
		public var gender:int = 0;
		/**
		 * 聊天频道
		 * @see core.consts.ChatChannelTypes
		 */		
		public var channel:int = 0;
		public function ChatItem()
		{
		}
	}
}