package core.formats
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * 避免在BitmapClip中重复的处理Resource对象，这里处理Resource对象并缓存
	 * @author Ado
	 * @see core.managers.BitmapResourceManager
	 */
	public class BitmapManagerItem
	{
		//资源名
		private var _name:String;
		//资源的位图序列
		private var _resource:Bitmap;
		//资源的位图数据缓存
		private var _bitmapdatas:Vector.<BitmapData>;
		//资源的配置
		private var _config:Object;
		//Tile的宽度
		private var unitWidth:int;
		//Tile的高度
		private var unitHeight:int;
		private var _mspf:int;
		private var _id:int;
		/**
		 * 
		 * @param src		资源的位图序列  Bitmap
		 * @param config	资源的配置信息，可以是对象或者XML，必须包含以下信息unitWidth
		 * 					(单位高度),unitHeight(单位宽度),frameRate(帧频,不是真正的帧频，是多少秒播
		 * 					放一帧),name(名字),id(唯一辨识)
		 * 
		 */		
		public function BitmapManagerItem(src:Bitmap, config:Object)
		{
			initConfig(config);
			resource = src;
		}
		/**
		 * 解析配置文件 
		 * @param config 
		 * 
		 */		
		private function initConfig(config:Object):void
		{
			_config = config;
			if(config is XML)
			{
				unitWidth = int(config.@unitWidth);
				unitHeight = int(config.@unitHeight);
				_mspf = int(config.@frameRate);
				_name = config.@name;
				_id = int(config.@id);
			}else
			{
				unitWidth = config.unitWidth;
				unitHeight = config.unitHeight;
				_mspf = config.frameRate;
				_name = config.name;
			}
		}
		/**
		 * 处理素材资源 
		 * 
		 */		
		private function initBitmapDatas():void
		{
			_bitmapdatas = new Vector.<BitmapData>();
			
			var hNo:int = resource.width/unitWidth;
			var vNo:int = resource.height/unitHeight;
			var rect:Rectangle = new Rectangle(0,0,unitWidth,unitHeight);
			var p:Point = new Point(0,0);
			for(var i:int = 0; i < hNo; i++)
			{
				for(var j:int = 0; j < vNo; j++)
				{
					rect.x = i * unitWidth;
					rect.y = j * unitHeight;
					var bd:BitmapData = new BitmapData(unitWidth, unitHeight);
					bd.copyPixels(resource.bitmapData,rect,p);
					_bitmapdatas.push(bd);
				}
			}
		}
		
		public function get resource():Bitmap
		{
			return _resource;
		}
		/**
		 * 初始位图资源 
		 * @param value
		 * 
		 */		
		public function set resource(value:Bitmap):void
		{
			_resource = value;
			initBitmapDatas();
		}
		/**
		 * 名字 
		 * @return 
		 * 
		 */		
		public function get name():String
		{
			return _name;
		}
		/**
		 * 处理好了的BitmapData资源序列 
		 * @return 
		 * 
		 */		
		public function get bitmapdatas():Vector.<BitmapData>
		{
			return _bitmapdatas;
		}

		public function get id():int
		{
			return _id;
		}
		/**
		 * 唯一标识符 
		 * @param value
		 * 
		 */		
		public function set id(value:int):void
		{
			_id = value;
		}
		/**
		 * miliseconds per frame：每帧耗费的时间 ms 
		 * @return 
		 * 
		 */		
		public function get mspf():int
		{
			return _mspf;
		}

	}
}