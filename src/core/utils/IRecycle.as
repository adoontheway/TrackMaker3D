package core.utils
{
	/**
	 *  回收接口
	 * @author Ado
	 * 
	 */	
	public interface IRecycle
	{
		/** 回收 */
		function recycle():void;
	}
}