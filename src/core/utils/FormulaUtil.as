package core.utils
{
	import flash.geom.Point;
	import flash.geom.Vector3D;

	/**
	 * @author Ado
	 */
	public class FormulaUtil
	{
		/**
		 * 根据两点确定一条直线，直线方程:y=a*x+b*z+c 
		 * @param p0
		 * @param p1
		 * @return {a:a, b:b, c:c}
		 */		
		public static function get3DLineFormula(p0:Vector3D, p1:Vector3D):Object
		{
			var a:Number = (p0.y-p1.y)/(p0.x-p1.x);
			var b:Number = (p0.y-p1.y - a*(p0.x - p1.x)) / (p0.z - p1.z);
			var c:Number = p0.y - a*p0.x - b*p0.z;
			return {a:a, b:b, c:c};
		}
		/**
		 * 根据两点求x-y平面上的直线方程 : y = k*x+b
		 * @param p0
		 * @param p1
		 * @return {k:k, b:b}
		 * 
		 */		
		public static function get2DLineFormula(p0:Point, p1:Point):Object
		{
			var k:Number = (p0.y-p1.y)/(p0.x-p1.x);
			var b:Number = p0.y - k * p0.x;
			return {k:k, b:b};
		}
		/**
		 * 根据圆上的3个点求圆的方程(x-a)^2+(y-b)^2=r^2 <br/>
		 * 原理两条线的法线的交汇处是圆心所在<br/>
		 * 直线方程:y = k*x+b
		 * @param p0
		 * @param p1
		 * @param p2
		 * @return {a:圆心的x坐标, b:圆心的y坐标, r:半径} 
		 */		
		public static function getCircleFormula(p0:Point, p1:Point, p2:Point):Object
		{
			var k0:Number = (p0.y - p1.y)/(p0.x - p1.x);
			var b0:Number = p0.y - k0 * p0.x;
			var fk0:Number = -1/k0;//p01的法线斜率
			var fb0:Number =  ((p0.y+p1.y)>>1) - fk0 *((p0.x+p1.x)>>1);//p01的法线b
			
			var k1:Number = (p0.y - p2.y)/(p0.x - p2.x);
			var b2:Number = p0.y - k1 * p0.x;
			var fk1:Number = -1/k1;
			var fb1:Number =  ((p0.y+p2.y)>>1) - fk1 *((p0.x+p2.x)>>1);//p02的法线b
			
			var a:Number = (fb1 - fb0)/(fk0 - fk1);
			var b:Number = fk0 * a + fb0;
			var disX:Number = a - p0.x;
			var disY:Number = b - p0.y;
			var r:Number = Math.sqrt(disX*disX+disY*disY);
			return{a:a,b:b,r:r};
		}
		
		public static function getRingFormula(vec0:Vector3D, vec1:Vector3D, vec2:Vector3D):Object
		{
			return new Vector3D();
		}
	}
}