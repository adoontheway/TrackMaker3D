package core.utils
{
	import flash.geom.Vector3D;

	public class MathUtil
	{
		public static function angleBetweenPoint(vec0:Vector3D,vec1:Vector3D):Number
		{
			return vec0.subtract(vec1).length;
		}
		
		public static function angleFromX(vec:Vector3D):Number
		{
			return 0;
		}
	}
}