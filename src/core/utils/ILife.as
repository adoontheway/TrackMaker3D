package  core.utils
{
	/**
	 * 生命体接口:<br/>
	 * 生命体是一些可用EnterFrame与Time处理的显示逻辑
	 * @author Ado
	 * @date Jul 12, 2012
	 */
	public interface ILife
	{
		/** 持续时间:生命行为持续时间 */
		function set duration(value:int):void;
		
		/** 生命行为:在EnterFrame里面调用 */
		function heartBeat():void;
		
		/** 重设 */
		function reset():void;
		
		/** 添加EnterFrame管理机制 */
		function addEnterFrameManager(manager:Object):void;
	}
}