package core.utils
{
	import core.errors.UtilClassInstanceError;

	/**
	 * 聊天频道类型
	 * @author Ado
	 * @see #core.errors.UtilClassInstanceError
	 */
	public class ChatChannel
	{
		/**
		 * 系统消息 
		 */		
		public static const SYSTEM_MSG:int = 0;
		/**
		 * 世界消息 
		 */		
		public static const WORLD_CHAT:int = 1;
		/**
		 * 门派消息 
		 */		
		public static const SCHOOL_CHAT:int = 2;
		/**
		 * 帮派消息 
		 */		
		public static const CLAN_CHAT:int = 3;
		/**
		 * 私人聊天 
		 */		
		public static const PRIVATE_CHAT:int = 4;
		/**
		 * 聊天频道标签 
		 */		
		public static var channelLabel:Object = ["系统","世界","门派","帮派","私人"];
		
		/**	频道色值 */		
		private static var _channelLabelColor:Array = [0xffff00,0x00ff00,0x00ffff,0xff00ff,0xffffff];
		/**
		 *  设置频道label在聊天框显示的颜色
		 * @param channel	0-4 不在范围内将会抛出异常
		 * @param color		频道label的颜色
		 * @throws Error:channel is not exsits...
		 */		
		public static function setColor(channel:int, color:uint):void
		{
			if(channel > 4 || channel < 0)
				throw new Error("channel is not exsits...");
			_channelLabelColor[channel] = color;
		}
		/**
		 * 取得频道标签的颜色 
		 * @param channel
		 * @return 颜色值
		 * 
		 */		
		public static function getColor(channel:int):uint
		{
			if(channel >= _channelLabelColor.length || channel < 0)
			{
				return 0xffffff;
			}else
			{
				return _channelLabelColor[channel];
			}
		}
		/**
		 * 请勿实例化 
		 * @throws UtilClassInstanceError
		 */		
		public function ChatChannel()
		{
			throw new UtilClassInstanceError("ChatChannelTypes");
		}
	}
}