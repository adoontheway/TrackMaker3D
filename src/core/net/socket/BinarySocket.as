package core.net.socket
{
	import core.managers.GlobalConfig;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.Socket;
	import flash.utils.ByteArray;

	/**
	 * 二进制通讯Socket，需要支持断线重连 
	 * @author Ado
	 * 
	 */	
	public class BinarySocket extends Socket
	{
//		private static var _chatSocket:BinarySocket;
		
		private static var _mainSocket:BinarySocket;
		
		public function BinarySocket(host:String=null, port:int=0)
		{
			super(host, port);
		}
		/**
		 * 生成包头 
		 * 
		 */		
		private function generateHeader(ba:ByteArray):void
		{
			this.writeUnsignedInt(ba.length);
		}
		/**
		 * 发送数据 
		 * @param data
		 * 
		 */		
		public function send(data:ByteArray):void
		{
			generateHeader(data);
			this.writeBytes(data);
			this.flush();
		}
		
		private function uninit():void
		{
			removeEventListener(Event.CONNECT, onConnect);
			removeEventListener(ProgressEvent.SOCKET_DATA, onData);
			removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurity);
			removeEventListener(IOErrorEvent.IO_ERROR, onError);
			removeEventListener(Event.CLOSE, onClose);
		}
		private function configure():void
		{
			addEventListener(Event.CONNECT, onConnect);
			addEventListener(ProgressEvent.SOCKET_DATA, onData);
			addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurity);
			addEventListener(IOErrorEvent.IO_ERROR, onError);
			addEventListener(Event.CLOSE, onClose);
		}
		private function onClose(e:Event):void
		{
			
		}
		private function onError(e:IOErrorEvent):void
		{
			
		}
		private function onSecurity(e:SecurityErrorEvent):void
		{
			
		}
		private function onData(e:ProgressEvent):void
		{
			//包长
			var len:int = this.readShort();
			if(len == 0)
			{
				return;
			}
			if(this.bytesAvailable < len)
			{
				
			}else
			{
				var protocol:int = this.readShort();
				var mark:int = this.readByte();
				if(mark >= 0)//成功
				{
					
				}else//失败
				{
					
				}
			}
			
		}
		private function onConnect(e:Event):void
		{
			var socket:BinarySocket = e.target as BinarySocket;
		}
		/**
		 * 初始化Socket连接,
		 * Socket的地址与端口在文件里面配置 
		 * 
		 */		
		public static function initSockets():void
		{
			if(GlobalConfig.serverIp == "" 
				|| GlobalConfig.serverPort == 0 
				|| GlobalConfig.chatIp == "" 
				|| GlobalConfig.chatPort == 0 )
			{
				throw new Error("Server config is not loaded or parsed correctly....");
			}
			mainSocket.configure();
			mainSocket.connect(GlobalConfig.serverIp,GlobalConfig.serverPort);
			
//			chatSocket.configure();
//			chatSocket.connect(GlobalConfig.chatIp,GlobalConfig.chatPort);
		}
		/**
		 * 游戏逻辑通讯Socket 
		 * @return 
		 * 
		 */		
		public static function get mainSocket():BinarySocket
		{
			if(_mainSocket == null)
			{
				_mainSocket = new BinarySocket();
			}
			return _mainSocket;
		}
		/**
		 * 聊天专用Socket 
		 * @return 
		 * 
		 */		
//		public static function get chatSocket():BinarySocket
//		{
//			if(_chatSocket == null)
//			{
//				_chatSocket = new BinarySocket();
//			}
//			return _chatSocket;
//		}
	}
}