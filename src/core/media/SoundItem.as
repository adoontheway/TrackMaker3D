package core.media
{
	import core.utils.IRecycle;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;

	/**
	 * 声音信息条 
	 * @example
	 * <listing>
	 * //创建一个SoundItem进行播放
	 * var sound:SoundItem = new SoundItem();
	 * sound.play("xxx.mp3",SoundPlayer.MUSIC,null);
	 * //重新播放
	 * sound.reset();
	 * //停止播放
	 * sound.stop();
	 * //回收
	 * sound.recyle();
	 * 
	 * </listing>
	 * 
	 * @author Ado
	 * @see SoundPlayer
	 */	
	internal class SoundItem extends EventDispatcher implements IRecycle
	{
		private var sound:Sound;
		private var transform:SoundTransform;
		private var channel:SoundChannel;
		//类型
		private var _type:int;
		//声音源
		private var _url:String;
		//当前的声音数量
		public static var num:int=0;
		//名字
		private var _name:String;
		//开始时间
		private var _startTime:int;
		//结束时间
		private var _endTime:int;
		//是否重复播放
		private var _repeat:int;
		public function SoundItem(target:IEventDispatcher=null)
		{
			super(target);
			sound = new Sound();
			num++;
			_name = "item"+num;
		}
		/**
		 * 播放声音 ，包括加载设置声音等等
		 * @param url		声音来源
		 * @param transform	声音转换
		 * @param startTime	开始时间
		 * @param endTime	结束时间
		 * @param repearTimes重复次数
		 * 
		 */		
		public function play(url:String="",type:int=0,transform:SoundTransform=null, startTime:int=0, repeatTimes:int=-1):void
		{
			_startTime = checkTime(startTime);
			//等于-1的话就无限重复
			_repeat = repeatTimes == -1 ? 999 : repeatTimes;
			
			this._type = type;
			this.transform = transform;
			if(sound.bytesLoaded == 0)//没有加载
			{
				_url = url;
				sound.load(new URLRequest(url));
				sound.addEventListener(Event.COMPLETE, onLoadComplete);
			}else if(url == "")//没有加载过或者加载失败
			{
				throw new Error("url of SoundItem " + this._name+" is empty...");
			}else//播放声音
			{
				playSound();
			}
		}
		/**
		 * 检查时间是否在规格范围内 
		 * @param num
		 * @return 
		 * 
		 */		
		private function checkTime(num:int):int
		{
			num = num < 0 ? 0 : num;
			num = num > sound.length ? sound.length : num;
			return num;
		}
		/**
		 * 播放声音 
		 * 
		 */		
		private function playSound():void
		{
			if(_repeat > 100)
			{
				sound.play(_startTime, _repeat, transform);
			}else
			{
				channel = sound.play(_startTime, _repeat, transform);
				if(channel)
					channel.addEventListener(Event.SOUND_COMPLETE, onPlayComplete);
			}
		}
		/**
		 * 声音加载完成 
		 * @param e
		 * 
		 */		
		private function onLoadComplete(e:Event):void
		{
			sound.removeEventListener(Event.COMPLETE, onLoadComplete);
			playSound();
		}
		/**
		 * 重置声音，从头开始播放 
		 * 
		 */		
		public function reset():void
		{
			sound.play(0, _repeat, transform);
		}
		/**
		 * 音乐重播 
		 * @param times
		 * 
		 */		
		public function repeat(times:int=0):void
		{
			_repeat = times;
			if(sound)
			{
				playSound();
			}else
			{
				trace("Sound is not exsits...");
			}
		}
		/**
		 * 停止声音 
		 * 
		 */		
		public function stop():void
		{
			channel.stop();
			channel.removeEventListener(Event.SOUND_COMPLETE, onPlayComplete);
			recycle();
		}
		/**
		 * 播放完成后进行回收 
		 * @param e
		 * 
		 */		
		private function onPlayComplete(e:Event):void
		{
			_repeat--;
			if(_repeat <= 0)
			{
				recycle();
				e.target.removeEventListener(Event.SOUND_COMPLETE, onPlayComplete);
			}else
			{
				playSound();
			}
		}
		/**
		 * 回收到SoundPlayer进行管理 
		 * 
		 */		
		public function recycle():void
		{
			channel.stop();
			SoundPlayer.recycle(this);
		}
		/**
		 * 声音的路径 
		 * @return 
		 * 
		 */		
		public function get url():String
		{
			return _url;
		}

		public function get type():int
		{
			return _type;
		}
		/**
		 * 名字 
		 * @return 
		 * 
		 */		
		public function get name():String
		{
			return _name;
		}
	}
}