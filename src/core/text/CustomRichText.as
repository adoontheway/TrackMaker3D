package core.text
{
	import core.utils.ChatChannel;
	import core.events.TextEventDispatcher;
	import core.formats.ChatItem;
	import core.managers.GlobalConfig;
	import core.net.BinaryLoader;
	
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.engine.ContentElement;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontWeight;
	import flash.text.engine.GraphicElement;
	import flash.text.engine.GroupElement;
	import flash.text.engine.TextBlock;
	import flash.text.engine.TextElement;
	import flash.text.engine.TextLine;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.getDefinitionByName;

	/**
	 * 自定义的图文混排框架，配合ChatItem使用
	 * @author Ado
	 * @see core.formats.ChatItem
	 * 
	 * @example
	 * <listing version="3.0">
	 * //初始化和添加到显示列表
	 * var customText:CustomRichText = new CustomRichText(100.100);
	 * addChild(customText);
	 * 
	 * //这个数据一般是服务端发送过来的
	 * var item:ChatItem = new ChatItem();
	 * item.name = "Someone";
	 * item.id = 110;
	 * item.content = "i don't know nothing...";
	 * item.gender = 0;
	 * item.channel = 1;
	 * //解析游戏格式
	 * customText.parseChatItem(item);
	 * 
	 * //解析通用格式
	 * customText.appendRichText("some thing you wanna display here.");
	 * </listing>
	 */
	public class CustomRichText extends Sprite
	{
		//表情分隔符
		private static var SPLITOR:String = "#";
		//用来建立元素的文本行
		private var textLine:TextLine;
		//文本行的y坐标
		private var yPos:Number;
		//所有建立起来的文本行
		private var lines:Vector.<TextLine>;
		
		//姓名的文本修饰
		private var nameDescription:FontDescription;
		//姓名文本格式
		private var nameFormat:ElementFormat;
		//普通的文本修饰
		private var normalDescription:FontDescription;
		//普通文本格式
		private var normalFormat:ElementFormat;
		
		private var _configXML:XML;
		/**
		 * 图文显示框的宽度和高度 
		 * @param dw	宽度
		 * @param dh	高度
		 * 
		 */		
		public function CustomRichText(dw:Number,dh:Number)
		{
			super();
			this.width = dw;
			this.height = dh;
			lines = new Vector.<TextLine>();
			yPos = 50;
			
			nameDescription = new FontDescription("Arial",FontWeight.BOLD);
			normalDescription = new FontDescription("Times New Roman");
			normalFormat = new ElementFormat(normalDescription,12,0x0000ff,1,"auto");
			nameFormat = new ElementFormat(nameDescription);
		}
		/**
		 * 设置图文框的宽度和高度 
		 * @param w
		 * @param h
		 * 
		 */		
		public function resizeSize(w:Number, h:Number):void
		{
			this.width = w;
			this.height = h;
		}
		/**
		 * 清空图文显示框 
		 * 
		 */		
		public function clear():void
		{
			for each(var tl:TextLine in lines)
			{
				removeChild(tl);
			}
		}
		/**
		 * 解析单条聊天信息 
		 * @param item	聊天信息
		 * @see core.formats.ChatItem
		 * @see core.utils.ChatChannel
		 */		
		public function parseChatItem(item:ChatItem):void
		{
			var elements:Vector.<ContentElement> = new Vector.<ContentElement>();
			
			//解析频道内容
			var description:FontDescription = new FontDescription("Arial","bold");
			var systemFormat:ElementFormat = new ElementFormat(description);
			if(item.channel == 0)//系统消息
			{
				systemFormat.color = ChatChannel.getColor(0);
				var element:TextElement = new TextElement("【"+ChatChannel.channelLabel[0]+"】:",systemFormat);
			}else if(item.channel == 1)//世界
			{
				systemFormat.color = ChatChannel.getColor(1);
				element = new TextElement("【"+ChatChannel.channelLabel[0]+"】:",systemFormat);
			}else if(item.channel == 2)//门派
			{
				systemFormat.color = ChatChannel.getColor(2);
				element = new TextElement("【"+ChatChannel.channelLabel[0]+"】:",systemFormat);
			}else if(item.channel == 3)//帮派
			{
				systemFormat.color = ChatChannel.getColor(3);
				element = new TextElement("【"+ChatChannel.channelLabel[0]+"】:",systemFormat);
			}else//私人
			{
				systemFormat.color = ChatChannel.getColor(4);
				element = new TextElement(item.name+"对你说:",systemFormat);
			}
			
			//解析信息发送者的信息
			if(item.id == GlobalConfig.me.id)//本人消息
			{
				description = new FontDescription("Arial");
				var format:ElementFormat = new ElementFormat(description);
				element = new TextElement("您说：",format);
			}else if(item.id)//玩家消息
			{
				description = new FontDescription("Arial");
				format = new ElementFormat(description);
				var eventMirror:TextEventDispatcher = new TextEventDispatcher(TextEventDispatcher.PLAYER,item.id);
				if(item.gender)
				{
					var color:int = 0xff0000;
				}else
				{
					color= 0x00ff00;
				}
				format.color = color;
				element = new TextElement(item.name,format);
				
				configureEvents(eventMirror);
			}
			
			//解析里聊天内容
			var content:String = item.content;
			appendRichText(content);
		}
		/**
		 * 为文本事件收发器配置事件监听 
		 * @param eventDispatcher
		 * @see core.events.TextEventDispatcher
		 */		
		private function configureEvents(eventDispatcher:EventDispatcher):void
		{
			eventDispatcher.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			eventDispatcher.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			eventDispatcher.addEventListener(MouseEvent.CLICK, onClick);
		}
		private function onMouseOver(e:MouseEvent):void
		{
			Mouse.cursor = MouseCursor.BUTTON;
		}
		private function onMouseOut(e:MouseEvent):void
		{
			Mouse.cursor = MouseCursor.ARROW;
		}
		private function onClick(e:MouseEvent):void
		{
			var target:TextEventDispatcher = e.target as TextEventDispatcher;
			var type:int = target.type;
			var data:Object = target.data;
		}
		/**
		 * 追加富文本 
		 * @param str
		 * 
		 */		
		public function appendRichText(str:String):void
		{
			var elements:Vector.<ContentElement> = new Vector.<ContentElement>();
			
			if(str == "") return;
			
			var index:int = str.indexOf(SPLITOR);
			if(index!=-1)
			{
				while(index!=-1)
				{
					if(str.length > index+2)
					{
						var faceIndex:int=parseInt(str.charAt(index+1)+str.charAt(index+2));
						if(isNaN(faceIndex))
						{
							var tempStr:String = str.slice(0,index);
							var textElement:TextElement = new TextElement(tempStr,normalFormat);
							elements.push(textElement);
							str = str.substr(index);
							index = str.indexOf(SPLITOR);
							continue;
						}else if(faceIndex)//判断是否在范围内，不在范围内同上处理
						{
							tempStr = str.slice(0,index+3);
							textElement = new TextElement(tempStr,normalFormat);
							elements.push(textElement);
							str = str.substr(index+3);
							index = str.indexOf(SPLITOR);
							continue;
						}
						if(index!=0)
						{
							tempStr = str.slice(0,index);
							textElement = new TextElement(tempStr,normalFormat);
							elements.push(textElement);
						}
					}else
					{
						break;
					}
					str = str.substr(index+3);
					index = str.indexOf(SPLITOR);
				}
				if(str!="")
				{
					textElement = new TextElement(tempStr,normalFormat);
					elements.push(textElement);
				}
			}else{
				textElement = new TextElement(tempStr,normalFormat);
				elements.push(textElement);
			}
			parseElements(elements);
		}
		/**
		 * 解析元素 
		 * @param elements
		 * 
		 */		
		private function parseElements(elements:Vector.<ContentElement>):void
		{
			var block:TextBlock = new TextBlock();
			
			if(elements.length!=1)
			{
				var group:GroupElement = new GroupElement(elements);
				block.content = group;
			}else if(elements.length == 0)
			{
				return;
			}else
			{
				var element:ContentElement = elements[0];
				block.content = element;
			}
			textLine = block.createTextLine(textLine,200);
			while(textLine)
			{
				textLine.x = 10;
				yPos += textLine.height+5;
				textLine.y = yPos;
				addChild(textLine);
				lines.push(textLine);
				textLine = block.createTextLine(textLine,200);
			}
		}
		/**
		 * 表情的配置文件 
		 * @return 
		 * 
		 */		
		public function get config():XML
		{
			return _configXML;
		}

		public function set config(value:XML):void
		{
			_configXML = value;
		}
	}
}