package core.events
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	/**
	 * 特殊的FTE文本事件映射器，包含了事件的类型与信息
	 * @author Ado
	 * @see core.text.CustomRichText
	 */

	public class TextEventDispatcher extends EventDispatcher
	{
		/**
		 * 物品 
		 */		
		public static const ITEM:int = 1;
		/**
		 * 玩家 
		 */		
		public static const PLAYER:int = 0;
		/**
		 * 宠物 
		 */		
		public static const CHARACTER:int = 2;
		//事件关联的类型：物品或者宠物或者人物
		private var _type:int;
		//事件关联的数据
		private var _data:Object;
		public function TextEventDispatcher($type:int, $data:Object=null, target:IEventDispatcher=null)
		{
			super(target);
			_type = $type;
			_data = $data;
		}
		/**
		 * 关联的事件类型 
		 * @return text显示条的类型
		 * 
		 */		
		public function get type():int
		{
			return _type;
		}
		/**
		 * 关联的事件信息 
		 * @return text包含的信息
		 * 
		 */		
		public function get data():Object
		{
			return _data;
		}
	}
}