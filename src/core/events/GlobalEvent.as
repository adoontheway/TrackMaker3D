package core.events
{
	/**
	 * @author Ado
	 * @name GlobalEvent
	 * @date Jun 27, 2012
	 */
	import flash.events.Event;
	
	public class GlobalEvent extends Event
	{
		/**
		 * 清理面板 
		 */		
		public static const EXLUDE_PANELS:String = "exclude_panels";
		/**
		 * 声音事件类型：静默模式 
		 */		
		public static const SILIENCE_MODEL:String = "silience_model";
		/**
		 * 选中表情，附带数据表情字符串 
		 */		
		public static const FACE_SELECTED:String = "face_selected";
		
		private var _data:Object;
		public function GlobalEvent(type:String, datumn:Object=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_data = datumn;
		}
		override public function clone():Event
		{
			return new GlobalEvent(type,bubbles,cancelable);
		}
		override public function toString():String
		{
			return "[GlobalEvent(type:"+type+", bubbles:"+bubbles+")]";
		}
		public function get data():Object
		{
			return _data;
		}

	}
}