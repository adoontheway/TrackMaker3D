package core.events
{
	/**
	 * 全局公用的事件发送者
	 * @author Ado
	 */
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class GlobalEventDispatcher extends EventDispatcher
	{
		private static var _instance:GlobalEventDispatcher;
		public function GlobalEventDispatcher(target:IEventDispatcher=null)
		{
			super(target);
		}
		public static function getInstance():GlobalEventDispatcher
		{
			if(null == _instance)
			{
				_instance = new GlobalEventDispatcher();
			}
			return _instance;
		}
	}
}