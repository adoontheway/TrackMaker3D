package core.display.panel
{
	
	import core.managers.LoaderManager;
	import core.managers.PanelManager;
	import core.consts.PanelTypes;
	import core.consts.ResourceId;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * 所有面板类的根类，里面实现了面板的显示，隐藏，添加/删除面板元素的事件监听器等,面板类型参考PanelTypes
	 * @author Ado
	 * @see core.consts.PanelTypes
	 **/
	public class BasePanel extends Sprite implements IView
	{
		protected var _isShow:Boolean;
		/** 主逻辑面板 */
		protected var mainPanel:Sprite;
		/** 新手引导id */
		protected var _cmdIds:Array = [];
		/**
		 * 面板类型：默认值<code>managers.PanelTypes.SHARE</code>
		 * @see  managers#PanelTypes
		 */		
		public var panelType:String = PanelTypes.SHARE;
		public function BasePanel()
		{
			super();
			initPanel();
			parseCmdIds();
		}
		/**
		 * 初始化面板，子类需要重写这个方法以加载mainPanel实体并将mainPanel实体添加到this，设置宽度和高度等一系列属性
		 * 
		 */		
		protected function initPanel():void
		{
			
		}
		/**
		 * 添加面板所有元素的事件 
		 */		
		public function addEvents():void
		{
			
		}
		/**
		 * 销毁面板
		 */		
		public function destroy():void
		{
			
		}
		/**
		 * 解析面板上的新手引导元件的id 
		 */		
		protected function parseCmdIds():void
		{
			var num:int = mainPanel.numChildren;
			for(var i:int=0; i < num; i++)
			{
				var child:DisplayObject = mainPanel.getChildAt(i);
				if(child.hasOwnProperty("cmdId"))
				{
					_cmdIds.push(child["cmdId"]);
				}
			}
		}
		/**
		 * 隐藏面板,包括逻辑如下
		 * <li>设置isShow为false</li>
		 * <li>通过PanelManager移除本面板</li>
		 * <li>移除面板元素的事件监听</li>
		 * <li>解除面板的新手引导控制</li>
		 * <li></li>
		 * <li></li>
		 */		
		public function hide():void
		{
			_isShow = false;
			_offset.x=0;
			_offset.y=0;
			removeEvents();
			
			PanelManager.removePanel(this);
			if(mainPanel)
				PanelManager.newbieParser.uncontrolPanel(mainPanel);
		}
		/**
		 * 移除面板元素的事件
		 * */
		public function removeEvents():void
		{
			
		}
		/**
		 * 面板显示，其中包含的逻辑
		 * <li>设置面板的isShow属性为true</li>
		 * <li>通过PanelManager将面板添加到舞台</li>
		 * <li>添加面板元素事件</li>
		 * <li>进行面板的新手引导控制</li>
		 * @param data 传给面板的数据
		 * */
		public function show(data:Object=null):void
		{
			_isShow = true;
			PanelManager.addPanel(this);
			addEvents();
			PanelManager.newbieParser.controlPanel(mainPanel);
		}
		/**
		 * 取得面板上含有的cmdId
		 * */
		public function get cmdIds():Array
		{
			return _cmdIds;
		}
		
		protected var _offset:Point = new Point();
		/**
		 * 面板对于初始位置的偏移量 
		 * @return 
		 * 
		 */		
		public function get offset():Point
		{
			return _offset;
		}
		public function set offset(value:Point):void
		{
			_offset = value;
			PanelManager.newbieParser.updatePosition(_offset,this);
		}
		
		/**
		 * 面板是否在显示 
		 * 
		 */
		public function get isShow():Boolean
		{
			return _isShow;
		}
	}
}