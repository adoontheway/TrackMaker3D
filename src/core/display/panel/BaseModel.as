package  core.display.panel
{
	import core.net.socket.BinarySocket;

	/**
	 * 模块Model的根类，用于存储数据，操作数据，以及向后台通讯
	 * @author Ado
	 **/
	public class BaseModel implements IModel
	{
		public function BaseModel()
		{
		
		}
		
		public function sendMsg(msg:Object):void
		{
			
		}
		
	}
}