package core.display.panel
{
	/**
	 * 控制器的接口，主要用于显示面板 
	 * @author Ado
	 * 
	 */	
	public interface IController
	{
		/**
		 * 显示面板 
		 * @param str	面板名
		 * @param data	传给面板的数据
		 * 
		 */		
		function showPanel(str:String,data:Object=null):void;
	}
}