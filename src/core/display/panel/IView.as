package  core.display.panel
{
	/**
	 * UI界面的接口
	 * @author Ado
	 **/
	public interface IView
	{
		/**
		 * 显示 
		 * 
		 */		
		function show(data:Object=null):void;
		/**
		 * 隐藏 
		 * 
		 */		
		function hide():void;
		/**
		 * 销毁 
		 * 
		 */		
		function destroy():void;
		/**
		 * 面板元素添加事件 
		 * 
		 */		
		function addEvents():void;
		/**
		 * 面板元素移除事件 
		 * 
		 */		
		function removeEvents():void;
	}
}