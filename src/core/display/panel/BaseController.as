package  core.display.panel
{
	import flash.display.Loader;
	
	/**
	 * 模块的控制器的根类,主要用于控制面板的显示逻辑
	 * @author Ado
	 **/
	public class BaseController implements IController
	{
		public function BaseController()
		{
			
		}
		
		public function showPanel(str:String,data:Object=null):void
		{
			
		}
		
	}
}