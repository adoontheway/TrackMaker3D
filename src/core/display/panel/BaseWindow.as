package core.display.panel
{
	import core.events.GlobalEvent;
	import core.events.GlobalEventDispatcher;
	import core.managers.CloseButtunIdManager;
	import core.managers.LoaderManager;
	import core.managers.PanelManager;
	import core.consts.ResourceId;
	
	import fl.managers.ToolTipManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 *	窗口面板的根类，窗口面板携带了标题，背景，拖动条，关闭按钮，群体关闭按钮
	 * 	@author Ado
	 * 
	 */	
	public class BaseWindow extends BasePanel 
	{
		/**
		 * 面板的唯一名字：一般是完整路径加类名，可用于Controller里面的showPanel()方法
		 * @see #BaseController.showPanel(name:String,data:Object) 
		 */		
		public static const NAME:String = "";
		
			
		/**
		 * 通用背景面板，含有背景，关闭按钮，拖动条 
		 */		
		protected var bg:Sprite;
		/**
		 * 通用背景面板上面的拖动条 
		 */		
		private var dragBar:Sprite;
		/**
		 * 面板上的标题文本 
		 */		
		protected var title:Sprite;
		/**
		 * 面板上的关闭按钮 
		 */		
		private var closeButton:Sprite;
		/**
		 * 群组关闭按钮 
		 */		
		private var allCloseButton:Sprite;
		public function BaseWindow()
		{
			super();
			//初始化界面元素
			init();
			//本地化语言界面
			localize();
			
		}
		/**
		 * 初始化面板的一些通用的显示结构
		 * <li>从通用组件库拿取背景bg</li>
		 * <li>从资源管理器中拿取mainPanel显示内容</li>
		 * <li>设置背景的宽度和高度，closeButton的位置，titleLabel的位置,mouseChildren,mouseEnabled,dragBar的长度</li>
		 * <li>设置mainPanel的位置</li>
		 * <li>进行语言版本国际化控制<code> IntelnationalUtils.getValue(key:String):String</code></li>
		 * <li>新手控制</li>
		 * remark:考虑bg的公用
		 */		
		protected function init():void
		{ 
			bg = LoaderManager.getObjectFromLoader("BasePanel", ResourceId.MAINBAR) as Sprite;
			
			closeButton = bg["closeBtn"];
			ToolTipManager.getInstance().registerToolTip(closeButton,"关闭");
			allCloseButton = bg["closeAll"];
			ToolTipManager.getInstance().registerToolTip(allCloseButton,"关闭所有面板");
			title = bg["titleLabel"];
			dragBar = bg["dragBar"];
			dragBar.alpha=0;
			title.mouseEnabled = false;
			title.mouseChildren = false;
			title["text"] = "面板";
			title.x = 8;
			title.y = 5;
			
			mainPanel.x = 5;
			mainPanel.y = 25;
			closeButton.x = mainPanel.x+mainPanel.width-closeButton.width;
			allCloseButton.x = closeButton.x - allCloseButton.width - 5;
			dragBar.width=closeButton.x;
			dragBar.height=25;
			bg["bg"].width = mainPanel.width + 10;//两边的间隙 5 px
			bg["bg"].height = mainPanel.height + 30;//上面保持25 px的间隙，下面5 px的间隙
			addChild(bg);
			
			addChild(mainPanel);
			
			this.x = (PanelManager.GlobalWidth - this.width)/2;
			this.y = (PanelManager.GlobalHeight - this.height)/2;
			
			closeButton["cmdId"]= CloseButtunIdManager.getCloseButtonIdByName(mainPanel.name);
			cmdIds.push(closeButton["cmdId"]);
		}
		/**
		 * 设置群组关闭按钮显示与否 
		 * @param visual Boolean 是否显示
		 * 
		 */		
		public function setAllCloseVisual(visual:Boolean):void
		{
			allCloseButton.visible = visual;
		}
		/**
		 * 初始化本地语言
		 * @see LanguageConvertor
		 * 
		 */		
		protected function localize():void
		{
			
		}
		/**
		 * 添加计时器管理 
		 * @param timer 计时器
		 * 
		 */		
		final public function addTimerManager(timer:Object):void
		{
			
		}
		/**
		 * 移除计时器管理 
		 * 
		 */		
		final public function removeTimerManager():void
		{
			
		}
		/**
		 * 添加帧控制管理 
		 * @param framer 帧管理器
		 * 
		 */		
		final public function addFrameManager(framer:Object):void
		{
			
		}
		/**
		 * 移除帧控制管理 
		 * 
		 */		
		final public function removeFrameManager():void
		{
			
		}
		/**
		 * 添加到舞台：空方法，保留备用 
		 * 
		 */		
		public function addToStage():void
		{
			
		}
		/**
		 * 关闭按钮的事件监听器 
		 * 
		 */		
		protected function closeHandler(e:MouseEvent):void
		{
			hide();
		}
		/**
		 * 在舞台上展示: addChild or visible = true
		 * 需要用相关工具进行管理，以便进行相关缓动动画 ,居中处理 etc
		 * @param data 传给面板的数据
		 */		
		override public function show(data:Object=null):void
		{
			super.show();
			PanelManager.newbieParser.controlPanel(bg);
		}
		/**
		 * 从舞台移除，移除方式:removeChild or visible=false 
		 * 需要用相关工具进行管理，以便舞台上剩余面板进行缓动调整位置
		 */		
		override public function hide():void
		{
			super.hide();
			removeEvents();
			PanelManager.newbieParser.uncontrolPanel(bg);
		}
		//x方向上的位移
		private var dx:Number;
		//y方向上的位移
		private var dy:Number;
		//参考位置的X
		private var referX:Number;
		//参考位置的Y
		private var referY:Number;
		
		/**
		 * 面板移动事件监听：主要用来发布新手引导元件的移动 
		 * @param e
		 * 
		 */		
		protected function onDragMove(e:MouseEvent=null):void
		{
			if(referX == 0 && referY == 0)
			{
				referX = this.x;
				referY = this.y;
			}
			dx = this.x - referX;
			dy = this.y - referY;
			referX = this.x;
			referY = this.y;
			_offset.x += dx;
			_offset.y += dy;
			if(dx == 0 && dy == 0) return;
			PanelManager.newbieParser.updatePosition(new Point(dx,dy),this);
		}
		
		/**
		 * 拖动条的鼠标按下事件 
		 * @param e
		 * 
		 */		
		protected function onDragDown(e:MouseEvent):void
		{
			referX = this.x;
			referY = this.y;
			var rect:Rectangle = new Rectangle(0,0,PanelManager.GlobalWidth-width,PanelManager.GlobalHeight-height);
			this.startDrag(false,rect);
			//据以往经验，弹起监听对象考虑使用Stage监听
			dragBar.addEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			dragBar.removeEventListener(MouseEvent.MOUSE_DOWN, onDragDown);
			dragBar.addEventListener(MouseEvent.ROLL_OUT, onDragOut);
			dragBar.addEventListener(MouseEvent.MOUSE_MOVE, onDragMove);
		}
		/**
		 * 鼠标移除拖动条之外的事件 
		 * @param e
		 * 
		 */		
		private function onDragOut(e:MouseEvent):void
		{
			this.stopDrag();
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			dragBar.addEventListener(MouseEvent.MOUSE_DOWN, onDragDown);
			dragBar.removeEventListener(MouseEvent.MOUSE_MOVE, onDragMove);
			dragBar.removeEventListener(MouseEvent.ROLL_OUT, onDragOut);
		}
		/**
		 * 拖动条的鼠标弹起事
		 * @param e
		 * 
		 */		
		protected function onStopDrag(e:MouseEvent):void
		{
			this.stopDrag();
			dragBar.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			dragBar.addEventListener(MouseEvent.MOUSE_DOWN, onDragDown);
			dragBar.removeEventListener(MouseEvent.MOUSE_MOVE, onDragMove);
		}
		/**
		 * 群组关闭按钮的事件监听器 
		 * @param e
		 * 
		 */		
		private function closeAllHandler(e:MouseEvent):void
		{
			GlobalEventDispatcher.getInstance().dispatchEvent(new GlobalEvent(GlobalEvent.EXLUDE_PANELS));
		}
		/**
		 * 添加事件监听器 
		 * 用来为mainPanel上面的组件添加事件监听器
		 */		
		override public function addEvents():void
		{
			dragBar.addEventListener(MouseEvent.MOUSE_DOWN, onDragDown);
			closeButton.addEventListener(MouseEvent.CLICK, closeHandler);
			allCloseButton.addEventListener(MouseEvent.CLICK, closeAllHandler);
		}
		/**
		 * 移除事件监听器 
		 * 用来为mainPanel上面的组件移除事件监听器
		 */		
		override public function removeEvents():void
		{
			dragBar.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			dragBar.removeEventListener(MouseEvent.MOUSE_DOWN, onDragDown);
			closeButton.removeEventListener(MouseEvent.CLICK, closeHandler);
			dragBar.removeEventListener(MouseEvent.MOUSE_MOVE, onDragMove);
			allCloseButton.removeEventListener(MouseEvent.CLICK, closeAllHandler);
		}
		/**
		 * 销毁 ：
		 * 销毁面板数据，面板事件，面板引用等，等待垃圾回收
		 */		
		override public function destroy():void
		{
			removeEvents();
		}
	}
}