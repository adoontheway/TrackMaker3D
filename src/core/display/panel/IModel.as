package  core.display.panel
{
	/**
	 * Model的接口，主要用于发送数据 
	 * @author Ado
	 * 
	 */	
	public interface IModel
	{
		/**
		 * 发送数据 
		 * @param msg	需要发送到后台的数据
		 * 
		 */		
		function sendMsg(msg:Object):void;
	}
}