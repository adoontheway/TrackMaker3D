package core.display
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.getTimer;
	
	import core.utils.ILife;
	import core.managers.BitmapResourceManager;
	import core.formats.BitmapManagerItem;
	/**
	 * 位图剪辑:enterframe负责播放，timer负责校正
	 * @author Ado
	 * @example 位图剪辑使用范例
	 * <listing>
	 * 	//初始化，参数为id
	 * 	var clip:BitmapClip = new BitmapClip(1);
	 * 
	 * 	//设置持续时间，0代表重复播放，单位ms	
	 * 	clip.duration = 0;
	 * 
	 * 	//添加帧管理器
	 * 	clip.addEnterFrameManager();
	 * 
	 * 	//重头播放
	 * 	clip.reset();
	 * 
	 * 	//停止播放
	 * 	clip.stop();
	 * 
	 * 	//播放指定帧
	 * 	clip.gotoAndStop(1);
	 * </listing>
	 */
	public class BitmapClip extends Bitmap implements ILife
	{
		/** 当前帧 */
		private var currentFrame:int;
		/** 每帧花费的ms数量 miliseconds per frame */
		private var mspf:int;
		/** 位图数据序列 */
		private var bitmapdatas:Vector.<BitmapData>;
		/** 开始时间 */
		private var _startTime:int;
		
		private var _duration:int=0;
		
		private var _id:int;
		public function BitmapClip($id:int)
		{
			super();
			id = $id;
		}
		/** 更新bitmap */
		private function updateBitmaps():void
		{
			var item:BitmapManagerItem = BitmapResourceManager.getBitmapResourceItem(_id);
			if(item)
			{
				bitmapdatas = item.bitmapdatas;
				mspf = item.mspf;
			}else
			{
				throw new Error("BitmapResourceManager didn't found resource [id : "+_id+"]");
			}
		}
		/**
		 * 播放指定帧 
		 * @param frame
		 * 
		 */		
		public function gotoAndPlay(frame:int=0):void
		{
			var index:int =frame%bitmapdatas.length;
			if(index != currentFrame)
			{
				currentFrame = index;
				this.bitmapData = bitmapdatas[index];
			}
		}
		/**
		 * 停止播放 
		 * 
		 */		
		public function stop():void
		{
			//removeEnterFrameManager
		}
		
		/**
		 * 添加帧管理器 
		 * @param manager
		 * 
		 */		
		public function addEnterFrameManager(manager:Object):void
		{
			heartBeat();
		}
		/**
		 * 生命行为 
		 * 
		 */		
		public function heartBeat():void
		{
			if(_startTime == 0)
			{
				_startTime = getTimer();
			}
		
			var time:int = getTimer() - _startTime;
			if( _duration != 0 && time > _duration)
			{
				stop();
			}else
			{
				//此处计算待商榷
				var index:int = Math.floor(time/mspf);
				gotoAndPlay(index);
			}
		}
		/**
		 * 重设 
		 * 
		 */		
		public function reset():void
		{
			_startTime = getTimer();
		}
		/**
		 * id 对应图片资源管理器里面的配置信息<br/>
		 * 待处理：如果没有缓存的话需要用默认图片代替
		 * @return 
		 * 
		 */		
		public function get id():int
		{
			return _id;
		}
		
		public function set id(value:int):void
		{
			_id = value;
			updateBitmaps();
		}

		public function get duration():int
		{
			return _duration;
		}
		/**
		 * 持续时间 
		 * @param value 为0的话无限播放下去，非0的话超时则停止
		 * 
		 */		
		public function set duration(value:int):void
		{
			_duration = value;
		}

	}
}