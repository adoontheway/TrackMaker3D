package  core.managers
{
	import flash.display.Loader;
	import flash.utils.Dictionary;
	
	import core.utils.LocalCache;
	import core.formats.SharedItem;

	/**
	 * 加载器管理器，用于存放ui资源,loader的id存放于ResourceId
	 * @author Ado
	 * @see core.formats.SharedItem
	 * @see core.utils.LocalCache
	 * @see core.consts.ResourceId
	 * @example
	 * <listing>
	 * var loader:Loader = new Loader;
	 * loader.loader(new URLRequest("someresource.swf"));
	 * loader.loaderInfo.addEventListener(Event.COMPLETE, onComplete)
	 * function onComplete(e:Event):void
	 * {
	 * 	//添加Loader到管理器里面
	 * 	LoaderManager.addLoader(1, loader);
	 * }
	 * 
	 * //从管理器里面拿loader，没有存则拿到null
	 * var temp:Loader = LoaderManager.getLoader(1);
	 * 
	 * //从id为的Loader里面拿到名字为aa的元素
	 * var obj:Object = LoaderManager.getObjectFromLoader("aa",1);
	 * </listing>
	 **/
	public class LoaderManager
	{
		/** 加载器的存储字典 */
		private static var infos:Dictionary = new Dictionary();
		/**
		 * 添加加载器，以SharedItem方式存放到本地
		 * @param id	存放的id
		 * @param loader需要存档的Loader
		 * 
		 */		
		public static function addLoader(id:int, loader:Object):void
		{
			var key:String = id.toString();
			var si:SharedItem = new SharedItem();
			si.version = GlobalConfig.VERSION;
			si.key = key;
			si.data = loader;
			LocalCache.saveToLocal(si);
//			infos[id] = loaderinfo;
		}
		/**
		 * 根据id取得加载器 
		 * @param id	当初存的id
		 * @return Loader
		 * 
		 */		
		public static function getLoader(id:int):Loader
		{
			var si:SharedItem = LocalCache.getDataFromCache(id.toString());
			if(si)
			{
				return Loader(si.data);
			}/**
			if(infos.hasOwnProperty(id))
			{
				return infos[id]
			}*/
			return null;
		}
		/**
		 * 从Loader取得物件 
		 * @param str 需要拿到的元素的名字
		 * @param lid 目标Loader
		 * @return 	  基本上是一个DisplayObject
		 * 
		 */		
		public static function getObjectFromLoader(str:String, lid:int):Object
		{
			var loader:Loader = getLoader(lid);
//			if(infos.hasOwnProperty(lid))
			if(loader)
			{
//				var loader:Loader = infos[lid];
				if(loader.contentLoaderInfo.applicationDomain.hasDefinition(str))
				{
					var claz:Class = loader.contentLoaderInfo.applicationDomain.getDefinition(str) as Class;
					return new claz;
				}else
				{
					trace(str+"不存在于"+lid+"#LoaderInfo中...");
				}
			}else
			{
				trace(lid+"#LoaderInfo 不存在....");
			}
			return null;
		}
	}
}