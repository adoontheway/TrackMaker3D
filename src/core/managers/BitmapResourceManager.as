package core.managers
{
	import core.errors.UtilClassInstanceError;
	import core.formats.BitmapManagerItem;
	
	import flash.display.Bitmap;
	import flash.utils.Dictionary;

	/**
	 * 图片资源管理器，配合BitmapManagerItem使用,用于BitmapClip
	 * 工具类，不可以实例化
	 * @throw UtilClassInstanceError
	 * @author Ado
	 * @see core.errors.UtilClassInstanceError
	 */
	public class BitmapResourceManager
	{
		private static var data:Dictionary = new Dictionary();
		public function BitmapResourceManager()
		{
			throw new UtilClassInstanceError("BitmapResourceManager");
		}
		/**
		 * 添加图片资源 
		 * @param src		图片资源
		 * @param config	配置信息，可以是xml或者Object
		 * @see	core.formats.BitmapManagerItem.BitmapManagerItem()
		 * 
		 */		
		public static function addBitmapResource(src:Bitmap,config:Object):void
		{
			var item:BitmapManagerItem = new BitmapManagerItem(src,config);
			data[item.id] = item;
		}
		/**
		 * 根据id取得图片资源 
		 * @param id
		 * @return 从缓存中拿取BitmapManagerItem，没有则返回Null
		 * 
		 */		
		public  static function getBitmapResourceItem(id:int):BitmapManagerItem
		{
			if(data.hasOwnProperty(id))
			{
				return data[id] as BitmapManagerItem;
			}
			return null;
		}
	}
}