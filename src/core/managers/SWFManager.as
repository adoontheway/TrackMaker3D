package core.managers
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;

	/**
	 * 用BinaryLoader记在的swf的管理器
	 * @author Ado
	 * @see core.net.BinaryLoader
	 */
	public class SWFManager
	{
		/**
		 * 缓存的swf字典 
		 */		
		private static var swfs:Dictionary = new Dictionary(); 
		/**
		 * 添加swf的字节流
		 * @param id	存放的id
		 * @param ba	需要存放当的二进制数据
		 * 
		 */		
		public static function addSwf(id:int, ba:ByteArray):void
		{
			swfs[id]=ba;
		}
		/**
		 * 是否有swf 
		 * @param id 需要检查的swf的id
		 * @return 
		 * 
		 */		
		public static function hasSwf(id:int):Boolean
		{
			return swfs.hasOwnProperty(id);
		}
		/**
		 * 根据id取得缓存的swf二进制数据 
		 * @param id
		 * @return 
		 * 
		 */		
		public static function getSwf(id:int):ByteArray
		{
			if(swfs.hasOwnProperty(id))
			{
				return swfs[id] as ByteArray;
			}
			return null;
		}
	}
}