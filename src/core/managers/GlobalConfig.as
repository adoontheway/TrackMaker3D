package core.managers
{
	import core.formats.User;

	/**
	 * 项目的配置信息，包括版本号和相关配置文件的路径
	 * @author Ado
	 * 
	 */
	public class GlobalConfig
	{
		/**
		 * 当前版本号 
		 */		
		public static var VERSION:int = 0;
		
		////////////////////路径相关///////////////////////
		/**
		 * 资源路径 
		 */		
		public static var RESOURCE_PATH:String = "resource/";
		/**
		 * 物品图标资源路径 
		 */		
		public static var ITEM_ICO_PATH:String = "resource/images/item_icos/";
		/**
		 * 玩家图标资源路径 
		 */		
		public static var PLAYER_ICO_PATH:String = "resource/images/player_icos/";
		/**
		 * 表情配置文件 
		 */		
		public static var FACE_PATH:String = "";
		
		///////////////////服务器相关//////////////////////
		/**
		 * 服务器ip 
		 */		
		public static var serverIp:String = "";
		/**
		 * 服务器端口号 
		 */		
		public static var serverPort:int = 0;
		/**
		 * 聊天ip 
		 */		
		public static var chatIp:String = "";
		/**
		 * 聊天端口号 
		 */		
		public static var chatPort:int = 0;
		
		///////////////////用户相关///////////////////////////
		/**
		 * 当前用户 
		 */		
		public static var me:User;
		/**
		 * 男性
		 */		
		public static const MALE:int = 0;
		/**
		 * 女性
		 */		
		public static const FEMALE:int = 1;
	}
}