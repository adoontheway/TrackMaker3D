package managers
{
	/**
	 * @author Ado
	 */
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class EventManager extends EventDispatcher
	{
		private static var _instance:EventManager;
		
		public function EventManager(target:IEventDispatcher=null)
		{
			super(target);
		}
		public static function getInstance():EventManager
		{
			if(_instance == null)
			{
				_instance = new EventManager();
			}
			return _instance;
		}
	}
}